# POCA 2020

Our product is a marketplace of videogames such as Instant-Gaming for instance.

## Install instructions

To run the software locally, Docker and postgresql is needed.

In addition, scala, sbt and terraform are needed for development.

### Create the database

To connect: `sudo -u postgres psql`

```
postgres=# create database poca;
CREATE DATABASE
postgres=# create user poca with encrypted password 'poca';
CREATE ROLE
postgres=# grant all privileges on database poca to poca;
GRANT
postgres=# \connect poca
You are now connected to database "poca" as user "postgres".
poca=# alter schema public owner to poca;
ALTER SCHEMA
```

In `pg_hba.conf`, make sure there is a way to connect as poca:
* `local poca poca md5` to connect using `psql`
* `host poca poca 127.0.0.1/32 md5` to connect using TCP.

Restart the database. Test the connection with `psql poca poca`.

If you plan to run tests, you need to create another database `pocatest`.


## Run the tests

```
sbt clean coverage test coverageReport
```

## Run the software

### Use the software online

Go to http://52.47.62.7/index

### Run locally using the Docker image from Docker Hub

```
docker run poca/poca-2020:latest
```

### Run from the local directory

```
sbt run
```

Then visit `http://localhost:8080/hello`

## Package to a Docker image

```
sbt docker:publishLocal
```

Then the image with name `poca-2020` and tag `latest` is listed. (There is also an image `poca-2020:0.1.0-SNAPSHOT` that is identical).

```
docker image ls
```

Run the docker image locally:

```
docker run --net=host poca-2020:latest
```

To remove old images:

```
docker image prune
```

## Deployment

First of all you need to type this on your shell :

```
export TF_VAR_a_key="our access key"
export TF_VAR_s_key="our secret key"
export TF_VAR_db_password="our db password"
```

In the directory `/terraform`, to initialize the project:

```
terraform init -backend-config="access_key=<our_access_key>" -backend-config="secret_key=<our_secret_key>"
```

To plan the deployment :

```
terraform plan --var-file=integration.tfvars -lock=false
```

To deploy :

```
terraform apply --var-file=integration.tfvars -lock=false
```

To destroy :

```
terraform destroy --var-file=integration.tfvars -lock=false
```

## Logs

Logs are stored on AWS Cloudwatch: https://eu-west-3.console.aws.amazon.com/cloudwatch/home?region=eu-west-3#logsV2:log-groups/log-group/poca-web/log-events
