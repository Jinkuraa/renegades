/*
The AWS user used by terraform is granted the AWS managed policy AdministratorAccess.
*/

terraform {
  backend "s3" {
    bucket = "pocabucket"
    key = "poca-2020"
    region = "eu-west-3"
    dynamodb_table = "poca-tfstates-locks"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.7.0"
    }
  }
}

variable "a_key" { // access_key
  description = "The access key for the aws provider."
  type        = string
}
variable "s_key" { // secret_key
  description = "The secret key for the aws provider."
  type        = string
}

provider "aws" {
  region = "eu-west-3"  # Europe (Paris)
  access_key = var.a_key
  secret_key = var.s_key
}

data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
