# PlayFrameWork

* https://www.playframework.com/documentation/2.0/api/scala/play/api/package.html
* https://www.playframework.com/documentation/2.8.x/JavaSessionFlash
* https://stackoverflow.com/questions/40243917/scala-play-how-to-update-main-scala-html-html-to-show-hide-text
* https://www.youtube.com/watch?v=U4w2Ya-HwMw&ab_channel=MarkLewis
* https://stackoverflow.com/questions/8205067/how-do-i-change-the-default-port-9000-that-play-uses-when-i-execute-the-run
  
What we need to modify in build.sbt :

* lazy val root = Seq ( (project in file(".")).enablePlugins(PlayScala) )
* libraryDependencies += guice
* libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test

And we need those plugins in project/plugins.sbt :

* addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.5")
* addSbtPlugin("org.foundweekends.giter8" % "sbt-giter8-scaffold" % "0.11.0")

It seems that PlayFrameWork use a specified port 9000, we probably are going to need to readapt the code.