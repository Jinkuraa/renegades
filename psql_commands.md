##CONNECTION

sudo -u postgres psql // To connect with the postgres user
psql poca poca // To connect to the poca database with the poca user

##DATABASE MANAGEMENT

select datname from pg_database; // To display all the databases
create database my_base; // To create the database my_base
drop database my_base; // To delete the database my_base

##USER MANAGEMENT

select usename from pg_user; // To display all users
create user foo; // To create the user foo
drop user foo; // To delete the user foo

##FIND THE CONFIGURATION FILE

show config_file

##USEFUL COMMANDS

\l = To display the list of databases
\q = To quit
\h = To display help
\c my_base = To connect to the my_base database
\d = To display the list of tables (after connecting to the database)