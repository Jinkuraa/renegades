﻿# Coding style
This coding style is extremely inspired by the Scala Style Guide on Scala documentation website.
The difference are :
* This coding style is shorter, I tried to go to the essential
* Not everything was covered
* I rephrased a lot of parts in order to make them more comprehensible from my point of view
* I took a lot of the examples from the Scala Style Guide but modified some in order to make them more comprehensible as well

# Indentation

## Line Wrapping
* Line length : 80 characters
* When you want to skip line :
	* the successive lines should be indented by two spaces
	* the current line ends with an infix method missing the right parameter
	or with a missing right parenthesis
``` scala
// first line ended with an infix method missing the right parameter
val result = 1 + 2 + 3 + 4 + 5 + 6 +
  7 
// first line ended with a missing right parenthesis
val result = (1 + 2
+ 3)
// that doesn't work, result value will be equal to 3 and not 6
val result = 1 + 2
+ 3
```

## Methods with Numerous Arguments
* Skip lines when you have like four or more arguments
 ```scala
foo(
  someVeryLongFieldName,
  andAnotherVeryLongFieldName,
  "this is a string",
  3.1415)
```

* Avoid invoking your methods at the end of a line if there are numerous arguments
``` scala
// good way
val myLongFieldNameWithNoRealPoint =
  foo(
    someVeryLongFieldName,
    andAnotherVeryLongFieldName,
    "this is a string",
    3.1415)

// identation would be painful for you
val myLongFieldNameWithNoRealPoint = foo(someVeryLongFieldName,
                                         andAnotherVeryLongFieldName,
                                         "this is a string",
                                         3.1415)
```


# Naming conventions
Notes : 
* UpperCamelCase : each word is capitalized
* lowerCamelCase : each word is capitalized except the first word

## UpperCamelCase 
Use UpperCamelCase when you name :

* Classes
* Traits
* Objects
* Constant names

## LowerCamelCase
* Methods
* Variables
* Values
* Annotations


## Package 
Use lowercase only,  with consecutive words simply concatenated together unless it is a root package or a class.
If the code is being developed within an organization which has a website, it should be the following format convention : `<top-level-domain>.<domain-name>.<project-name>`
```java
`com.example.deepspace`, not `com.example.deepSpace` or `com.example.deep_space`
// root package
import _root_.net.liftweb._
// import everything from the users package
import users._  
// import the class User
import users.User  
```

## Accessor/Mutators

* The accessor has the name of the property it is accessing 
``` scala
def age = ...
```
If it is a boolean you can add "is" in front of it like in this : ``` isEmpty```

* The mutator has the name of the property it is accessing followed by '_' 
``` scala
def age_=(new_val : Int) {
	...
}
```

## Methods of arity-0
* If the method is an accessor, you should not put ```()``` after its name unless it has side effects.
```scala
// doesn't change state, call as birthdate
def birthdate = firstName

// has side effects
def age() = {
  _age = updateAge(birthdate)
  _age
}
```
* If the method is not an accessor, you can do as you wish

## Type Parameters (generics)
* For simple type parameters, you should use a single uppercase letter (English alphabet, not exotic ones :)). 
It starts from A.
```scala
class List[A] {
  def map[B](f: A => B): List[B] = ...
}
```
* If the type is specific, you can be more descriptive by writing the word fully, partially, or use its first letter.
If you decide to write the word fully or partially, the first letter is an uppercase letter while the rest of the letters are lowercase letters.

``` scala
class Map[Key, Value] {
  def get(key: Key): Value
  def put(key: Key, value: Value): Unit
}

class Map[K, V] {
  def get(key: K): V
  def put(key: K, value: V): Unit
}

// all-caps so it is not good
class Map[KEY, VALUE] {
  def get(key: KEY): VALUE
  def put(key: KEY, value: VALUE): Unit
}
```

## Functionnal style
* Your local names and parameters can be short
```scala
def add(a: Int, b: Int) = a + b
```

# Types
## Inference

* Do not annotate private field or local variables and use inference instead.
``` scala
// we can deduce the type of name by its value
private val name = "Daniel"
```
* Add type annotations to your public methods.
* If Scala knows the type of the function value we are declaring, you don't need to annotate the parameters.
```scala
val ls: List[String] = List("1", "2")
/* Scala knows the function will give a List of int from strings, so it knows that str is a string
*/
val lsInt = ls map (str => str.toInt)
```

## Functions

* Function types should be declared with a space between the parameter type, the arrow and the return type:

``` scala
def foo(f: Int => String) = ...

def bar(f: (Boolean, Double) => List[String]) = ...
```

## Structural Types
* You can declare structural types on a single line if they are less than 50 characters in length,
otherwise you have to skip lines.
``` scala
// wrong!
def foo(a: { def bar(a: Int, b: Int): String; val baz: List[String => String] }) = ...

// right!
private type FooParam = {
  val baz: List[String => String]
  def bar(a: Int, b: Int): String
}

def foo(a: FooParam) = ...

// inline and less than 50 characters
def foo(a: { val bar: String }) = ...
```

# Nested Blocks

## Curly Braces
*   Opening brace on the same line as the declaration
*  One space before the opening brace.
*   Line break after the opening brace.
*   Line break before the closing brace.
```scala
def foo = {
  ...
}
```

## Parentheses
* If your parenthetical block wrap across lines, the opening and closing parentheses should be unspaced and kept on the same lines as their content.
```scala
// good
(this + is a very ++ long *
  expression)
  
// bad
(
this + is a very ++ long *
  expression
)
```

## Declarations

# Classes

* Class/Object/Trait constructors should be declared all on one line, unless the line length > 80. In that case, put each constructor argument on its own line with trailing commas.
* Open a parenthesis after the name of the class, and after you write your last attribute, skip a line and
close the parenthesis to provides visual separation between constructor arguments and extensions.
* Add an empty line to separate extensions from class implementation: :

``` scala
class Person(
  name: String,
  age: Int,
  birthdate: Date,
  astrologicalSign: String,
  shoeSize: Int,
  favoriteColor: java.awt.Color,
) extends Entity
  with Logging
  with Identifiable
  with Serializable {

  def firstMethod: Foo = …
}
```

## Ordering of class elements
* All class/object/trait members should be declared interleaved with newlines. The only exceptions to this rule are `var` and `val`. These may be declared without the intervening newline, but only if none of the fields have Scaladoc and if all of the fields have simple (max of 20-ish chars, one line) definitions
``` scala
class Foo {
// no newline between the two val because they are val and their definition is short
// a newline between the two def
  val bar = 42   
  val baz = "Daniel"

  def doSomething(): Unit = { ... }

  def add(x: Int, y: Int): Int = x + y
}
```
* Fields should _precede_ methods in a scope. The only exception is if the `val` has a block definition (more than one expression) and performs operations which may be deemed “method-like” (e.g. computing the length of a `List`). This val may be declared at a later point in the file.

## Methods
* Methods should be declared according to the following pattern :
``` scala
def foo(p1: p1Type, ...): returnType = expr
```

* If they have default parameter values, add a space before and after the equal sign.
``` scala
def foo(x: Int = 6, y: Int = 7): Int = x + y
```

* You can omit the return type of local or private methods.
``` scala
private def foo(x: Int = 6, y: Int = 7) = x + y
```

## Body

When the body of a method cannot be concisely expressed in a single line or is of a non-functional nature (some mutable state, local or otherwise), the body must be enclosed in braces :

``` scala
def sum(ls: List[String]): Int = {
  val ints = ls map (_.toInt)
  ints.foldLeft(0)(_ + _)
}
```

Methods which contain a single `match` expression should be declared in the following way :
``` scala
def sum(ls: List[Int]): Int = ls match {
  case hd :: tail => hd + sum(tail)
  case Nil => 0
}
```

## Control Structures
* Add a space after an if, else, for, while.
``` scala
if (foo) bar else baz
for (i <- 0 to 10) { ... }
while (true) { println("Hello, World!") }
```

## Trivial Conditionals
* If your expression is short you can have your if and else on the same line.
 ```scala
val res = if (foo) bar else baz
```


# Method Invocation

## Arity-0

Scala allows the omission of parentheses on methods of arity-0 :

``` scala
reply()

// is the same as

reply
```
* You can omit the parenthesis if the method in question has no side-effects (purely-functional). In other words, it would be acceptable to omit parentheses when calling `queue.size`, but not when calling `println()`.

## Symbolic Methods/Operators

Symbolic methods (operators) should always be invoked using infix notation with spaces separating the target, the operator, and the parameter

``` scala
// right!
"walid" + " " + "cristiano"
a + b

// wrong!
"walid"+" "+"cristiano"
a+b
a.+(b)
```

## Higher-Order Functions

* Methods which take functions as parameters (such as `map` or `foreach`) should be invoked using infix notation.
```scala
// wrong!
names.map { _.toUpperCase }.filter { _.length > 5 }

// right!
names map { _.toUpperCase } filter { _.length > 5 }
```

# Files

Companion objects should be grouped with their corresponding class or trait in the same file.

# Scaladoc

* Example of Scaladoc for a class
```scala
/** A person who uses our application.
 *
 *  @constructor create a new person with a name and age.
 *  @param name the person's name
 *  @param age the person's age in years
 */
class Person(name: String, age: Int) {
}
```

* Example of Scaladoc for an object
```scala
/** Factory for [[mypackage.Person]] instances. */
object Person {
  /** Creates a person with a given name and age.
   *
   *  @param name their name
   *  @param age the age of the person to create
   */
  def apply(name: String, age: Int) = {}

  /** Creates a person with a given name and birthdate
   *
   *  @param name their name
   *  @param birthDate the person's birthdate
   *  @return a new Person instance with the age determined by the
   *          birthdate and current date.
   */
  def apply(name: String, birthDate: java.util.Date) = {}
}
```

* Example of Scaladoc if an object does implicit conversions
```scala
/** Implicit conversions and helpers for [[mypackage.Complex]] instances.
 *
 *  {{{
 *  import ComplexImplicits._
 *  val c: Complex = 4 + 3.i
 *  }}}
 */
object ComplexImplicits {}
```

When only a simple, short description is needed, a one-line format can be used:
```scala
/** Does something very simple */
def simple: Unit = ()
```

* Example of Scaladoc for packages 
```scala
package my.package
/** Provides classes for dealing with complex numbers.  Also provides
 *  implicits for converting to and from `Int`.
 *
 *  ==Overview==
 *  The main class to use is [[my.package.complex.Complex]], as so
 *  {{{
 *  scala> val complex = Complex(4,3)
 *  complex: my.package.complex.Complex = 4 + 3i
 *  }}}
 *
 *  If you include [[my.package.complex.ComplexConversions]], you can
 *  convert numbers more directly
 *  {{{
 *  scala> import my.package.complex.ComplexConversions._
 *  scala> val complex = 4 + 3.i
 *  complex: my.package.complex.Complex = 4 + 3i
 *  }}}
 */
package complex {}
```

## General rules
* Get to the point quickly. (ex : say “returns true if some condition” instead of “if some condition return true”)
* Try to format the first sentence of a method as “Returns XXX" as in “Returns the first element of the List”.
* Try to format the first sentence of a class as "Does XXX".
* When referring to the instance of the class, use “this XXX”, or “this” and not “the XXX”. For objects, say “this object”.

# Sources
https://docs.scala-lang.org/style/index.html
https://google.github.io/styleguide/javaguide.html (for package convention)
