# Change log

* [#114](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/114) Session for my orders
* [#113](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/113) Promo page
* [#112](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/112) Adding Session into RoutesBucksShopWallet
* [#101](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/101) Implement promotion twirl
* [#95](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/95) Payments of the products of the cart
* [#91](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/91) Cart's products handling
* [#86](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/86) Personal user page
* [#85](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/85) ShoppingCart interface for the users and functionnalities transaction, receipt
* [#83](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/83) Add a way to close the session
* [#82](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/82) Integrate the session into signup
* [#81](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/81) Integrate the session into signin
* [#79](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/79) Integrate the session into the forum
* [#76](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/76) Make a page + design to buy "RenegadeBucks"
* [#62](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/62) Buy game Keys
* [#59](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/59) Add a virtual money shop
* [#53](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/53) Ability to close a forum topic
* [#52](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/52) Enhance forum page
* [#50](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/50) Manageabble error handling
* [#48](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/48) Client session
* [#47](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/47) Add a generic error page
* [#46](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/46) Navigbar on all main pages
* [#44](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/44) Ability to click on a forum topic and be redirected to the correct page
* [#43](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/43) Ability to create forum topics 
* [#41](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/41) Add search functionality
* [#38](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/38) Create a topic creation page for our website
* [#33](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/33) Improve user password security
* [#32](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/32) Reply button on forum
* [#31](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/31) Index page for forum
* [#30](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/30) Ability to post on forum properly (communication with DB)
* [#28](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/28) Create a forum
* [#25](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/25) Video game redirection to their description
* [#24](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/24) Add some video games in the index
* [#22](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/22) Create an index page for our website
* [#21](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/21) Add the partners in the index page
* [#18](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/18) Add a password input to the sign up page
* [#16](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/16) Deploy the application on AWS
* [#15](https://gaufre.informatique.univ-paris-diderot.fr/therenegades/poca-2020/issues/15) Add signin to index
* [#43](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/30) Allow registration of new users
* [#27](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/27) Deploy the software on the AWS cloud
* [#21](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/21) Publish to Docker Hub (poca/poca-2020)
* [#19](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/19) Build a Docker image during CI
* [#18](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/18) Package using Docker
* [#16](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/16) Add a webserver
* [#12](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/12) Setup code coverage
* [#6](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/8) Setup Continuous Integration
* [#4](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/4) Document the aim of the product
* [#3](https://gaufre.informatique.univ-paris-diderot.fr/michelbl/poca-2020/issues/3) Add a change log
