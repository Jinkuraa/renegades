-- Insert data in this file

--insert into users (user_id, is_admin, username, email, password, wallet) 
--values('azefadfvdrfze',false,'toto','toto@yoyo.com','dummy_password',0);

insert into products (product_id, product_name, release_date, description, price,
          os, processor, memory, graphics, storage, image, partner)
values ('cyb', 
'Cyberpunk 2077', 
'2020-11-19', 
'Cyberpunk 2077 is an open-world, action-adventure story set in Night City, a megalopolis obsessed with power, 
glamour and body modification.',
15,
'Windows 7 or 10', 
'Intel Core i5-3570K or AMD FX-8310', 
'8 GB RAM', 
'NVIDIA GeForce GTX 780 or AMD Radeon RX 470',
'70 GB available space', 
'resources/images/game_covers/cyberpunk-2077-cover.png',
'Steam'
);

insert into products (product_id, product_name, release_date, description, price,
          os, processor, memory, graphics, storage, image, partner)
values ('tw3wh', 
'The Witcher 3 : Wild Hunt', 
'2020-05-19', 
'The Witcher 3: Wild Hunt is a 2015 action role-playing game developed and published by Polish developer CD Projekt Red and 
is based on The Witcher series of fantasy novels written by Andrzej Sapkowski.',
15,
'64-bit Windows 7, 64-bit Windows 8 (8.1) or 64-bit Windows 10', 
'Intel CPU Core i5-2500K 3.3GHz / AMD CPU Phenom II X4 940', 
'6 GB RAM', 
'Nvidia GPU GeForce GTX 660 / AMD GPU Radeon HD 7870',
'35 GB available space', 
'resources/images/game_covers/the-witcher-3-wild-hunt-cover.png',
'Steam'
);

insert into products (product_id, product_name, release_date, description, price,
          os, processor, memory, graphics, storage, image, partner)
values ('hk', 
'Hollow Knight', 
'2017-02-24', 
'Forge your own path in Hollow Knight! An epic action adventure through a vast ruined kingdom of insects and heroes. 
Explore twisting caverns, battle tainted creatures and befriend bizarre bugs, all in a classic, 
hand-drawn 2D style.',
70,
'Windows 7', 
'Intel Core 2 Duo E5200', 
'4 GB RAM', 
'GeForce 9800GTX+ (1GB)',
'9 GB available space', 
'resources/images/game_covers/hollow-knight-cover.jpg',
'Steam'
);

insert into products (product_id, product_name, release_date, description, price,
          os, processor, memory, graphics, storage, image, partner)
values ('bsif', 
'Bioshock Infinite', 
'2013-03-25', 
'Indebted to the wrong people, with his life on the line, veteran of the U.S. Cavalry and now hired gun, 
Booker DeWitt has only one opportunity to wipe his slate clean. He must rescue Elizabeth, a mysterious girl 
imprisoned since childhood and locked up in the flying city of Columbia. Forced to trust one another, Booker and 
Elizabeth form a powerful bond during their daring escape. Together, they learn to harness an expanding arsenal of 
weapons and abilities, as they fight on zeppelins in the clouds, along high-speed Sky-Lines, and down in the streets 
of Columbia, all while surviving the threats of the air-city and uncovering its dark secret.',
10,
'Windows Vista Service Pack 2 32-bit', 
'Intel Core 2 DUO 2.4 GHz / AMD Athlon X2 2.7 GHz', 
'2 GB RAM', 
'DirectX10 Compatible ATI Radeon HD 3870 / NVIDIA 8800 GT / Intel HD 3000 Integrated Graphics',
'29 GB available space', 
'resources/images/game_covers/bioshock-infinite-cover.jpg',
'Steam'
);

insert into products (product_id, product_name, release_date, description, price,
          os, processor, memory, graphics, storage, image, partner)
values ('mhw', 
'Monster Hunter : World', 
'2018-08-9', 
'Monster Hunter: World for PC is an action role-playing game in which the player tracks and hunts various 
monsters and animals and mythical creatures using a variety of weapons. The game is played from the third person 
point of view.',
20,
'WINDOWS® 7, 8, 8.1, 10 (64-bit required)', 
'Intel® Core™ i5-4460, 3.20GHz or AMD FX™-6300', 
'8 GB RAM', 
'NVIDIA® GeForce® GTX 760 or AMD Radeon™ R7 260x (VRAM 2GB)',
'38 GB available space', 
'resources/images/game_covers/monster-hunter-world-cover.jpg',
'Steam'
);

insert into products (product_id, product_name, release_date, description, price,
          os, processor, memory, graphics, storage, image, partner)
values ('me1', 
'Mass Effect', 
'2007-11-20', 
'Mass Effect is an action role-playing video game developed by BioWare and published by Microsoft Game Studios. 
Originally released for the Xbox 360 video game console in 2007, it is the first game of the Mass Effect series. 
The game takes place within the Milky Way galaxy in the year 2183, where civilization is threatened by a highly 
advanced machine race known as the Reapers. The player assumes the role of Commander Shepard, an elite human 
soldier who must stop a rogue agent from carrying out the Reapers galactic invasion. The gameplay requires 
the player to complete multiple quests that generally involve space exploration, squad and vehicular combat, 
and interaction with non-player characters.',
20,
'Microsoft Windows® XP with SP2 or Windows Vista*',
'Intel P4 2.4 Ghz or faster / AMD 2.0 Ghz',
'1.0 GB RAM or more (2.0 GB for Vista)',
'DirectX 9.0c compatible, ATI X1300 XT or greater (ATI X1300, X1300 Pro, X1600 Pro, Radeon 2600 HD)',
'12.0 GB or more free hard drive space', 
'resources/images/game_covers/mass-effect-cover.jpg',
'Origin'
);


