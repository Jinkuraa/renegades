
package poca

import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._
import scala.io.Source

class Migration02RefactoTopics(db: Database) extends Migration with LazyLogging {
    override def apply(): Unit = {
        implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

        val removeColumn: Future[Int] = db.run(sqlu"ALTER TABLE topics DROP COLUMN last_msg;")
        Await.result(removeColumn, Duration.Inf)
    }
}
