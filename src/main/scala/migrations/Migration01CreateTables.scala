
package poca

import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._
import scala.io.Source 

class Migration01CreateTables(db: Database) extends Migration with LazyLogging {
    class CurrentUsersTable(tag: Tag) extends Table[(String, Boolean, String, String, String, Int)](tag, "users") {
        def user_id = column[String]("user_id", O.PrimaryKey)
        def is_admin = column[Boolean]("is_admin")
        def username = column[String]("username")
        def email = column[String]("email")
        def password = column[String]("password")
        def wallet = column[Int]("wallet")
        def * = (user_id, is_admin, username, email, password, wallet)
    }

    class CurrentProductsTable(tag: Tag) extends Table[(String, String, String, String, Float,
      String, String, String, String, String, String, String)](tag, "products") {
        def product_id = column[String]("product_id", O.PrimaryKey)
        def product_name = column[String]("product_name")
        def release_date = column[String]("release_date")
        def description = column[String]("description")
        def price = column[Float]("price")
        def os = column[String]("os")
        def processor = column[String]("processor")
        def memory = column[String]("memory")
        def graphics = column[String]("graphics")
        def storage = column[String]("storage")
        def image = column[String]("image")
        def partner = column[String]("partner")
        def * = (product_id, product_name, release_date, description, price,
          os, processor, memory, graphics, storage, image, partner)
    }

    class CurrentMailBoxTable(tag: Tag) extends Table[(String, String, String, String, String)](tag, "mailbox") {
        def message_id = column[String]("message_id", O.PrimaryKey)
        def user_id_send = column[String]("user_id_send")
        def user_id_receive = column[String]("user_id_receive")
        def content = column[String]("content")
        def date_hour = column[String]("date_hour")
        def * = (message_id, user_id_send, user_id_receive, content, date_hour)
    }

    class CurrentStoreTable(tag: Tag) extends Table[(String, String)](tag, "store") {
        def wallet = column[String]("wallet")
        def nb_of_sales = column[String]("nb_of_sales")
        def * = (wallet, nb_of_sales)
    }

    class CurrentTopicsTable(tag: Tag) extends Table[(String, String, String, String, String)](tag, "topics") {
        def topic_id = column[String]("topic_id", O.PrimaryKey)
        def user_id = column[String]("user_id")
        def subject = column[String]("subject")
        def author = column[String]("author")
        def last_msg = column[String]("last_msg")
        def * = (topic_id, user_id, subject, author, last_msg)
    }

    class CurrentMessagesTable(tag: Tag) extends Table[(String, String, String, String, String)](tag, "messages") {
        def message_id = column[String]("message_id", O.PrimaryKey)
        def topic_id = column[String]("topic_id")
        def user_id = column[String]("user_id")
        def content = column[String]("content")
        def date_hour = column[String]("date_hour")
        def * = (message_id, topic_id, user_id, content, date_hour)
    }

    override def apply(): Unit = {
        implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
        // users
        val users = TableQuery[CurrentUsersTable]
        val dbio: DBIO[Unit] = users.schema.create
        val creationFuture: Future[Unit] = db.run(dbio)
        Await.result(creationFuture, Duration.Inf)
        // Products
        val products = TableQuery[CurrentProductsTable]
        val dbioProducts: DBIO[Unit] = products.schema.create
        val creationFutureProducts: Future[Unit] = db.run(dbioProducts)
        Await.result(creationFutureProducts, Duration.Inf)
        // MailBox
        val mailbox = TableQuery[CurrentMailBoxTable]
        val dbioMailBox: DBIO[Unit] = mailbox.schema.create
        val creationFutureMailBox: Future[Unit] = db.run(dbioMailBox)
        Await.result(creationFutureMailBox, Duration.Inf)
        // Store
        val store = TableQuery[CurrentStoreTable]
        val dbioStore: DBIO[Unit] = store.schema.create
        val creationFutureStore: Future[Unit] = db.run(dbioStore)
        Await.result(creationFutureStore, Duration.Inf)
        // Topics
        val topics = TableQuery[CurrentTopicsTable]
        val dbioTopics: DBIO[Unit] = topics.schema.create
        val creationFutureTopic: Future[Unit] = db.run(dbioTopics)
        Await.result(creationFutureTopic, Duration.Inf)
        // Messages
        val messages = TableQuery[CurrentMessagesTable]
        val dbioMessages: DBIO[Unit] = messages.schema.create
        val creationFutureMessages: Future[Unit] = db.run(dbioMessages)
        Await.result(creationFutureMessages, Duration.Inf)
        
    }
}
