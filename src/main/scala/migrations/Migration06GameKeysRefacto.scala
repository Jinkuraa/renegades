
package poca

import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._
import scala.io.Source


class Migration06GameKeysRefacto(db: Database) extends Migration with LazyLogging {
  override def apply(): Unit = {
    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

    // Raneming column
    val renamingColumn: Future[Int] = db.run(sqlu"ALTER TABLE gamekeys RENAME COLUMN user_id TO username;")
    Await.result(renamingColumn, Duration.Inf)
  }
}
