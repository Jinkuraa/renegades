
package poca

import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._
import scala.io.Source


class Migration05UserAndStoreRefacto(db: Database) extends Migration with LazyLogging {
  override def apply(): Unit = {
    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

    // Refacto store
    val alterColumnWallet: Future[Int] = db.run(sqlu"ALTER TABLE store ALTER COLUMN wallet SET DATA TYPE real USING wallet::real;")
    Await.result(alterColumnWallet, Duration.Inf)

    val alterColumnNb_of_sales: Future[Int] = db.run(sqlu"ALTER TABLE store ALTER COLUMN nb_of_sales SET DATA TYPE integer USING nb_of_sales::integer;")
    Await.result(alterColumnNb_of_sales, Duration.Inf)

    val initStore: Future[Int] = db.run(sqlu"INSERT INTO store (wallet, nb_of_sales) VALUES (0 , 0);")
    Await.result(initStore, Duration.Inf)

    // Refacto user
    val changeWalletTypeColumn: Future[Int] = db.run(sqlu"ALTER TABLE users ALTER COLUMN wallet SET DATA TYPE REAL;")
    Await.result(changeWalletTypeColumn, Duration.Inf)
  }
}
