package poca

import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._


class Migration07CartTable(db: Database) extends Migration with LazyLogging {
  override def apply(): Unit = {
    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

    val createCart: Future[Int] = db.run(sqlu"CREATE TABLE cart ( user_id TEXT, product_id TEXT ); ")
    Await.result(createCart, Duration.Inf)

  }
}
