
package poca

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._
import org.postgresql.util.PSQLException

import scala.io.Source

trait Migration {
    def apply(): Unit
}

class RunMigrations(db: Database) extends LazyLogging {
    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
    val migrationList: List[Migration] = List(
        new Migration00AddVersionNumber(db),
        new Migration01CreateTables(db),
        new Migration02RefactoTopics(db),
        new Migration03CreateGameKeys(db),
        new Migration04GameKeysRefacto(db),
        new Migration05UserAndStoreRefacto(db),
        new Migration06GameKeysRefacto(db),
        new Migration07CartTable(db),
        new Migration08PromoTable(db),
    )

    def getCurrentDatabaseVersion(): Int = {
        val getVersionRequest: DBIO[Seq[Int]] = sql"select * from database_version;".as[Int]
        val responseFuture: Future[Seq[Int]] = db.run(getVersionRequest)

        val versionFuture = responseFuture.
            map(versionSeq => versionSeq(0)).
            recover{
                case exc: PSQLException => {
                    if (exc.toString.contains("ERROR: relation \"database_version\" does not exist")) {
                        0
                    } else {
                        throw exc
                    }
                }
            }
        
        val version = Await.result(versionFuture, Duration.Inf)
        logger.info(s"Database version is $version")
        version
    }

    def incrementDatabaseVersion(): Unit = {
        val oldVersion = getCurrentDatabaseVersion()
        val newVersion = oldVersion + 1

        val updateVersionRequest: DBIO[Int] = sqlu"update database_version set number = ${newVersion};"

        val updateVersionFuture: Future[Int] = db.run(updateVersionRequest)

        Await.result(updateVersionFuture, Duration.Inf)
        logger.info(s"Database version incremented from $oldVersion to $newVersion")
    }

    def apply() {
        val version = getCurrentDatabaseVersion()

        migrationList.slice(version, migrationList.length).foreach(migration => {
            migration()
            incrementDatabaseVersion()
        })
//        addMockData()
    }
    def addMockData (): Unit = {
        val inserts_sql = Source.fromResource("data/inserts.sql").mkString
        val addDataFuture: Future[Int] = db.run(sqlu"#$inserts_sql")
        Await.result(addDataFuture, Duration.Inf)
        logger.info("MOCK DATA INSERTED")
    }
}
