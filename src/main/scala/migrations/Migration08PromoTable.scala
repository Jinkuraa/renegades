package poca

import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._


class Migration08PromoTable(db: Database) extends Migration with LazyLogging {
  override def apply(): Unit = {
    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

    val createPromoTable: Future[Int] = db.run(sqlu"CREATE TABLE promo ( promo_id TEXT, products_id TEXT, promo NUMERIC (4, 2) DEFAULT 0, start_promo TEXT, end_promo TEXT, CHECK (promo >= 0 AND promo <= 100.0) );  ")
    Await.result(createPromoTable, Duration.Inf)

  }
}

