
package poca

import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._
import scala.io.Source

class Migration03CreateGameKeys(db: Database) extends Migration with LazyLogging {
    class CurrentGameKeysTable(tag: Tag) extends Table[(String, String, Int, Boolean, String, Float, String)](tag, "gamekeys") {
        def gamekeys_id = column[String]("gamekeys_id", O.PrimaryKey)
        def code = column[String]("code")
        def product_id = column[Int]("product_id")
        def available = column[Boolean]("available")
        def user_id = column[String]("user_id")
        def price = column[Float]("price")
        def purchase_date = column[String]("purchase_date")
        def * = (gamekeys_id, code, product_id, available, user_id, price, purchase_date)
    }

    override def apply(): Unit = {
        implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
        val gamekeys = TableQuery[CurrentGameKeysTable]
        val dbio: DBIO[Unit] = gamekeys.schema.create
        val creationFuture: Future[Unit] = db.run(dbio)
        Await.result(creationFuture, Duration.Inf)
    }
}
