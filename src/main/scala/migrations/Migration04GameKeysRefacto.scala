
package poca

import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._
import scala.io.Source


class Migration04GameKeysRefacto(db: Database) extends Migration with LazyLogging {
  override def apply(): Unit = {
    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
    val alterColumn: Future[Int] = db.run(sqlu"ALTER TABLE gamekeys DROP COLUMN product_id;")
    Await.result(alterColumn, Duration.Inf)
    val addColumn: Future[Int] = db.run(sqlu"ALTER TABLE gamekeys ADD product_id TEXT NOT NULL;")
    Await.result(addColumn, Duration.Inf)
    val removeColumn: Future[Int] = db.run(sqlu"ALTER TABLE gamekeys DROP COLUMN price;")
    Await.result(removeColumn, Duration.Inf)
  }
}
