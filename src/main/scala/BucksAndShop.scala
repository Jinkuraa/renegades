package poca

import scala.concurrent.Future
import com.typesafe.scalalogging._

final case class OfferDoesNotExistException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)

class BucksAndShop(val user: User, wallet: Users) extends PaymentMethod {

    // Offers (rb = RenegadeBucks)
    // 1000 rb = 9.99 €
    // 2800 rb = 24.99€ (12% bonus)
    // 5000 rb = 39.99€ (25% bonus)
    // 13500 rb = 99.99€ (35% bonus)
    
    /* 
        offer must be between 1 and 4
    */
    def addBucks(offer: Short): Future[Unit] = {
        offer match {
            case 1 => wallet.updateWallet(user.username, 1000f)
            case 2 => wallet.updateWallet(user.username, 2800f)
            case 3 => wallet.updateWallet(user.username, 5000f)
            case 4 => wallet.updateWallet(user.username, 13500f)
            case _ => throw OfferDoesNotExistException("Impossible, this offer does not exist.")
        }
    }

    def removeBucks(amount: Float): Future[Unit] = {
        wallet.updateWallet(user.username, -amount) 
    }

    @Override
    def pay(amount: Float, numberOfKeys: Int,store: Store = null) : Boolean = {
        if (user.wallet < amount) return false
        removeBucks(amount)
        if(store!=null)store.updateStore(amount, numberOfKeys)
        return true 
    }
}