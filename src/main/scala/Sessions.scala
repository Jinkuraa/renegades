
package poca

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive, Directive0, Directive1, ExceptionHandler}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.headers.{ Cookie, HttpCookie, `Set-Cookie`, HttpCookiePair }
import com.typesafe.scalalogging.LazyLogging

object Sessions extends LazyLogging {

    val id = "renegate_id"
    val name = "renegate_name"
    val all = (id, name)

    def set(user_id: String, username: String): Directive0 = {
        setCookie(HttpCookie(id, value = user_id), HttpCookie(name, value = username))
    }

    private def get_aux(s: String, ss: String*): Directive1[List[Option[HttpCookiePair]]] = {
        val dir = optionalCookie(s)
        if (ss.isEmpty) {
            dir.map(List(_))
        } else {
            dir.flatMap(c => get_aux(ss.head, ss.tail: _*).map(c::_))
        }
    }

    def getall(s: String, ss: String*): Directive1[Array[Option[HttpCookiePair]]] = {
        get_aux(s, ss: _*).map(_.toArray)
    }

    def clear(): Directive0 = {
        deleteCookie(id).tflatMap( _ => deleteCookie(name))
    }

}
