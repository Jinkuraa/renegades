
package poca

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
import java.util.UUID

case class User(user_id: String, is_admin: Boolean, username: String, email: String, password: String, wallet: Float)

final case class UserNotExistsException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)
final case class UserAlreadyExistsException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)
final case class InconsistentStateException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)
final case class AttributesAlreadyUsedByUserException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)

class Users {
    class UsersTable(tag: Tag) extends Table[(String, Boolean, String, String, String, Float)](tag, "users") {
        def user_id = column[String]("user_id", O.PrimaryKey)
        def is_admin = column[Boolean]("is_admin")
        def username = column[String]("username")
        def email = column[String]("email")
        def password = column[String]("password")
        def wallet = column[Float]("wallet")
        def * = (user_id, is_admin, username, email, password, wallet)
    }

    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
    val db = MyDatabase.db
    val users = TableQuery[UsersTable]

    def createUser(is_admin: Boolean, username: String, email: String, password: String, wallet: Float): Future[Unit] = {
        val existingUsersFuture = getUserByUsername(username)
        val existingUserEmailFuture = getUserByEmail(email)
        existingUsersFuture.flatMap(existingUsers => {
            existingUserEmailFuture.flatMap(existingEmailUsers => {
                (existingUsers,existingEmailUsers) match{
                    case (None, None) => {
                        val user_id = UUID.randomUUID.toString()
                        val encrypted_password = Hash.bcryptHash(password)
                        val newUser = User(user_id=user_id, is_admin=is_admin, username=username, email=email, password=encrypted_password, wallet=wallet)
                        val newUserAsTuple: (String, Boolean, String, String, String, Float) = User.unapply(newUser).get
                        val dbio: DBIO[Int] = users += newUserAsTuple
                        var resultFuture: Future[Int] = db.run(dbio)

                        // We do not care about the Int value
                        resultFuture.map(_ => ())
                    }
                    case (Some(_), Some(_)) => 
                        throw new AttributesAlreadyUsedByUserException("email,username")
                    case (Some(_), None) => 
                        throw new UserAlreadyExistsException(s"A user with username '$username' already exists.")
                    case (None, Some(_)) => 
                        throw new AttributesAlreadyUsedByUserException("email")
                }
            })
        })
    }

    private def getUserByQuery(query:
      Query[UsersTable,(String, Boolean, String, String, String, Float),Seq],
      parameter_value: String,
      parameter_name: String = "Parameter"): Future[Option[User]] = {
        val userListFuture = db.run(query.result)

        userListFuture.map((userList: Seq[(String, Boolean, String, String, String, Float)]) => {
            userList.length match {
                case 0 => None
                case 1 => Some(User tupled userList.head)
                case _ => 
                    throw new InconsistentStateException(s"$parameter_name " +
                    s"$parameter_value is linked to several users in database!")
            }
        })
    }

    def getUserByUsername(username: String): Future[Option[User]] = {
        val query = users.filter(_.username === username)
        getUserByQuery(query,username,"username")
    }

    def getUserByEmail(email: String): Future[Option[User]] = {
        val query = users.filter(_.email === email)
        getUserByQuery(query,email,"email")
    }

    def getUserById(id: String): Future[Option[User]] = {
        val query = users.filter(_.user_id === id)
        getUserByQuery(query,id,"id")
    }

    def getAllUsers(): Future[Seq[User]] = {
        val userListFuture = db.run(users.result)

        userListFuture.map((userList: Seq[(String, Boolean, String, String, String, Float)]) => {
            userList.map(User tupled _)
        })
    }

    def updateWallet(username : String, amount : Float): Future[Unit] = {
        val updateFuture: Future[Int] = db.run(sqlu"update users set wallet = wallet + ${amount} WHERE users.username = ${username};")
        updateFuture.map(_ => ())
    }
}
