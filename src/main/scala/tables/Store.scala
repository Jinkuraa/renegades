package poca

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
import java.util.UUID

case class StoreC(wallet : Float, nb_of_sales : Int)

class Store {
    class StoreTable(tag: Tag) extends Table[(Float, Int)](tag, "store") {
        def wallet = column[Float]("wallet")
        def nb_of_sales = column[Int]("nb_of_sales")
        def * = (wallet, nb_of_sales)
    }

    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
    val db = MyDatabase.db
    val store = TableQuery[StoreTable]

    def updateStore(addWallet : Float, nb_of_products : Int): Future[Unit] = {
        val updateFuture: Future[Int] = db.run(sqlu"update store set wallet = ((SELECT wallet FROM store) + ${addWallet}), nb_of_sales = ((SELECT nb_of_sales FROM store) + ${nb_of_products});")
        updateFuture.map(_ => ())
    }

    def getStore(): Future[StoreC] = {
        val storeFuture = db.run(store.result)

        storeFuture.map((storeList: Seq[(Float, Int)]) => {
            StoreC tupled storeList.head
        })
    }
}