package poca

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
import java.util.UUID

case class Message(message_id: String, topic_id: String, user_id: String, content: String, date_hour: String)

class Messages{

    class MessagesTable(tag: Tag) extends Table[(String, String, String, String, String)](tag, "messages") {
        def message_id = column[String]("message_id", O.PrimaryKey)
        def topic_id = column[String]("topic_id")
        def user_id = column[String]("user_id")
        def content = column[String]("content")
        def date_hour = column[String]("date_hour")
        def * = (message_id, topic_id, user_id, content, date_hour)
    }

    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

    val db = MyDatabase.db
    val messagesTable = TableQuery[MessagesTable]

    def createMessage(topic_id: String, user_id: String, content: String, date_hour: String): Future[Unit] = {
        val message_id = UUID.randomUUID.toString()
        val newMessage = Message(message_id = message_id, topic_id = topic_id, user_id = user_id, content = content, date_hour = date_hour)
        val newMessageAsTuple: (String, String, String, String, String) = Message.unapply(newMessage).get
        val dbio: DBIO[Int] = messagesTable += newMessageAsTuple
        var resultFuture: Future[Int] = db.run(dbio)
        resultFuture.map(_ => ())
    }

    def getMessagesByTopicId(topic_id: String): Future[Option[Seq[Message]]] = {
        val query = messagesTable.filter(_.topic_id === topic_id)

        val messageListFuture = db.run(query.result)

        messageListFuture.map((messageTuple: Seq[(String, String, String, String, String)]) => {
            messageTuple.length match {
                case 0 => None
                case _ => Some(messageTuple.map(Message tupled _))
            }
        })
    }

    def deleteMessagesByTopicId(topic_id: String): Future[Int] = {
        val query = messagesTable.filter(_.topic_id === topic_id)
        val action = query.delete
        db.run(action)
    }

    def getAllMessages(): Future[Seq[Message]] = {
        val messageListFuture = db.run(messagesTable.result)

        messageListFuture.map((messageTuple: Seq[(String, String, String, String, String)]) => {
            messageTuple.map(Message tupled _)
        })
    }
}
