
package poca

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
import java.util.UUID
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

case class GameKey(gamekeys_id: String, code : String, product_id : String, available : Boolean, 
username : String, purchase_date : String)

final case class KeyNotFoundException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)
final case class GameKeyAlreadyExistsException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)

class GameKeys {
    class GameKeysTable(tag: Tag) extends Table[(String, String, String, Boolean, String, String)](tag, "gamekeys") {
        def gamekeys_id = column[String]("gamekeys_id", O.PrimaryKey)
        def code = column[String]("code")
        def product_id = column[String]("product_id")
        def available = column[Boolean]("available")
        def username = column[String]("username")
        def purchase_date = column[String]("purchase_date")
        def * = (gamekeys_id, code, product_id, available, username, purchase_date)
    }

    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
    val db = MyDatabase.db
    val gamekeys = TableQuery[GameKeysTable]

    def createGameKey(code: String, product_id: String): Future[Unit] = {
        val products = new Products()
        val productFuture : Future[Option[Product]] = products.getProductById(product_id)
        productFuture.flatMap(product => product match {
            case None => 
                throw ProductNotFoundException(s"Product with product_id = ${product_id} not found !")
            case Some(_) => { 
                val existingGameKeyFuture = getGameKeyByCode(code)
                existingGameKeyFuture.flatMap(existingGameKeys => existingGameKeys match{
                    case Some(_) =>
                        throw GameKeyAlreadyExistsException(s"the code ${code} already exists for another GameKey")
                    case None => {
                        val gamekeys_id = UUID.randomUUID.toString()
                        val newGamekeys = GameKey(gamekeys_id = gamekeys_id, code = code, 
                        product_id=product_id, available=true, username="", purchase_date="")
                        val newGameKeysAsTuple: (String, String, String, Boolean, String, String) = 
                        GameKey.unapply(newGamekeys).get
                        val dbio: DBIO[Int] = gamekeys += newGameKeysAsTuple
                        var resultFuture: Future[Int] = db.run(dbio)
                        resultFuture.map(_ => ())
                    }
                })
            }
        })
    }
    
    def getGameKeyByCode(code: String): Future[Option[GameKey]] = {
        val query = gamekeys.filter(_.code === code)
        val gameKeyListFuture = db.run(query.result)

        gameKeyListFuture.map((gameKeyList: Seq[(String, String, String, Boolean, String, String)]) => {
        gameKeyList.length match {
                case 0 => None
                case 1 => Some(GameKey tupled gameKeyList.head)
                case _ => throw InconsistentStateException(
                          s"The code ${code} is linked to several GameKey in database!")
            }
        })
    }

    def isAvailableForThatProduct(product_id : String): Future[Boolean] = {
        val query = gamekeys.filter(_.product_id === product_id)
        val query2 = query.filter(_.available === true)
        val gameKeyListFuture = db.run(query2.result)
        gameKeyListFuture.map((gameKeyList: Seq[(String, String, String, Boolean, String, String)]) => {
        gameKeyList.length match {
                case 0 => false
                case _ => true
            }
        })       
    }

    def getAllGameKeysOwnedByUser(username : String): Future[Seq[(String, String, String, String)]] = {
        db.run(sql"SELECT distinct products.product_name, products.image, gamekeys.code, gamekeys.purchase_date  FROM ((products  INNER JOIN gamekeys ON products.product_id = gamekeys.product_id) INNER JOIN users ON  ${username} = gamekeys.username) order by purchase_date desc".as[(String, String, String, String)])
    }

    def getAllGameKeys(): Future[Seq[GameKey]] = {
        val gameKeysListFuture = db.run(gamekeys.result)
        gameKeysListFuture.map((gameKeysList: Seq[(String, String, String, Boolean, String, String)]) => {
            gameKeysList.map(GameKey tupled _)
        })
    }

    def getAvailableKeyForThatProduct(product_id : String): Future[Option[GameKey]] = {
        val query = gamekeys.filter(_.product_id === product_id)
        val query2 = query.filter(_.available === true)
        val gameKeyListFuture = db.run(query2.result)
        gameKeyListFuture.map((gameKeyList: Seq[(String, String, String, Boolean, String, String)]) => {
            Some(GameKey tupled gameKeyList.head)
        })
    }

    def buyKey(username : String, gamekeys_id : String): Future[Unit] = {
        val purchase_date = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm").format(LocalDateTime.now)
        val updateFuture: Future[Int] = db.run(sqlu"update gamekeys set available = false, username = ${username} , purchase_date = ${purchase_date} where gamekeys_id = ${gamekeys_id};")
        updateFuture.map(_ => ())
    }
} 
