
package poca

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
import java.util.UUID
import scala.concurrent.duration.Duration

case class Product(product_id : String, product_name :String, 
release_date : String, description : String, price : Float, os : String, processor : String, memory : String, 
graphics : String, storage : String, image : String, partner : String)

final case class ProductAlreadyExistsException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)
final case class ProductNotFoundException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)


class Products {
    class ProductsTable(tag: Tag) extends Table[(String, String, String, String, Float,
      String, String, String, String, String, String, String)](tag, "products") {
        def product_id = column[String]("product_id", O.PrimaryKey)
        def product_name = column[String]("product_name")
        def release_date = column[String]("release_date")
        def description = column[String]("description")
        def price = column[Float]("price")
        def os = column[String]("os")
        def processor = column[String]("processor")
        def memory = column[String]("memory")
        def graphics = column[String]("graphics")
        def storage = column[String]("storage")
        def image = column[String]("image")
        def partner = column[String]("partner")
        def * = (product_id, product_name, release_date, description, price,
          os, processor, memory, graphics, storage, image, partner)
    }

    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
    val db = MyDatabase.db
    val products = TableQuery[ProductsTable]

    def createProduct (product_id : String, product_name :String, release_date : String, description : String, price : Float, os : String, processor : String, memory : String, 
graphics : String, storage : String, image : String, partner : String) : Future[Unit] = {

      val productFuture: Future[Option[Product]] = getProductById(product_id)
      productFuture.flatMap(product => product match {
        case Some(p) =>
          throw new ProductAlreadyExistsException(s"The product with id = ${product_id} already exist")
        case None => {
          val newProduct = Product(product_id = product_id, product_name = product_name, release_date = release_date, 
          description = description, price = price, os = os, processor = processor, memory = memory, graphics = graphics,
          storage = storage, image = image, partner = partner)
          val newProductAsTuple: (String, String, String, String, Float,
          String, String, String, String, String, String, String) = Product.unapply(newProduct).get
          val dbio: DBIO[Int] = products += newProductAsTuple
          var resultFuture: Future[Int] = db.run(dbio)
          resultFuture.map(_ => ())
        }
      })
    }

    def getProductById(product_id: String): Future[Option[Product]] = {
        val query = products.filter(_.product_id === product_id)
        val productListFuture = db.run(query.result)

        productListFuture.map((productList: Seq[(String, String, String, String, Float,
      String, String, String, String, String, String, String)]) => {
        productList.length match {
                case 0 => None
                case 1 => Some(Product tupled productList.head)
                case _ => throw new InconsistentStateException(
                          s"${product_id} is linked to several products in database!")
            }
        })
    }

    def getAllProductsLike(likePattern : String) : Future[Seq[Product]] = {
        val query = products.filter(_.product_name.toLowerCase like "%" + likePattern + "%")
        val productListFuture = db.run(query.result)
        productListFuture.map((productList: Seq[(String, String, String, String, Float,
       String, String, String, String, String, String, String)]) => {
        productList.map(Product tupled _)
      })
    }
    
    def getAllProducts(): Future[Seq[Product]] = {
        val productListFuture = db.run(products.result)
        productListFuture.map((productList: Seq[(String, String, String, String, Float,
      String, String, String, String, String, String, String)]) => {
        productList.map(Product tupled _)
      })
    }
}
