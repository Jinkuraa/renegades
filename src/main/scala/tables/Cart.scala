
package poca

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
import com.typesafe.scalalogging.LazyLogging

case class CartElement(var user_id : String, var product_id : String)

final case class AlreadyInCartException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)

class Cart {
    class CartTable(tag: Tag) extends Table[(String, String)](tag, "cart") {
        def user_id = column[String]("user_id")
        def product_id = column[String]("product_id")
        def * = (user_id, product_id)
    }

    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
    val db = MyDatabase.db
    val cart = TableQuery[CartTable]

    def isInCart(user_id : String, product_id : String) : Future[Boolean] = {
        val query = cart.filter(_.product_id === product_id)
        val query2 = query.filter(_.user_id === user_id)
        val cartElementListFuture = db.run(query2.result)
        cartElementListFuture.map((cartElementList: Seq[(String, String)]) => {
        cartElementList.length match {
                case 0 => false
                case _ => true
            }
        })       
    }

    def addProductToCart(user_id : String, product_id : String) : Future[Unit] = {
        val isInCartFuture = isInCart(user_id, product_id)
        isInCartFuture.flatMap(_ match {
            case true => throw new AlreadyInCartException("Already in cart !")
            case false => 
                val newCartElement = CartElement(user_id = user_id, product_id = product_id)
                val newCartElementAsTuple : (String, String) = CartElement.unapply(newCartElement).get
                val dbio: DBIO[Int] = cart += newCartElementAsTuple
                var resultFuture: Future[Int] = db.run(dbio)
                resultFuture.map(_ => ())
        })
    }

    //to change so it can get the price
    def getCartFromUser(user_id : String) : Future[Seq[(String, String, Float, String)]] = {
        db.run(sql"SELECT products.product_name, products.image, products.price, products.product_id FROM (products  INNER JOIN cart ON products.product_id = cart.product_id and ${user_id} = cart.user_id)".as[(String, String, Float, String)])
    }

    def getCartIdsFromUser(user_id : String) : Future[Seq[String]] = {
        db.run(sql"SELECT products.product_id FROM (products  INNER JOIN cart ON products.product_id = cart.product_id and ${user_id} = cart.user_id)".as[String])
    }
    

    def getValueOfCartFromUser(user_id : String) : Future[Seq[(Float)]] = {
        db.run(sql"SELECT sum(products.price) FROM (products  INNER JOIN cart ON products.product_id = cart.product_id and ${user_id} = cart.user_id)".as[(Float)])
    }

    def removeProductFromCart(user_id : String, product_id: String): Future[Unit] = {
        val removeFuture = db.run(sql"DELETE FROM cart WHERE cart.user_id = ${user_id} and ${product_id} = cart.product_id".as[(Int)])
        removeFuture.map(_ => ())
    }

    
    private def handlePayment(elt:(String,String,Float,String), user_id: String, method: PaymentMethod, store:Store): Future[Unit]= {
        if(method.pay(elt._3,1,store)){
            removeProductFromCart(user_id, elt._4).map(_ => ())
        } else {
        Future()
    }
  }

   private def handlePromos(method: PaymentMethod,promos: List[(Seq[String],String)], promo: Promotions, store : Store): Future[List[String]] = {
        var promosPaid: List[String] = List()
        promos.foldLeft(Future(promosPaid)) {(f,elt)=>
            f.flatMap(l => {
                val promo_value = promo.giveTotalPriceWithPromotion(elt._2)
                promo_value.map(value => {
                method.pay(value,elt._1.size,store)
                l.appendedAll(elt._1)                     //here l is the accumulator list
                })
            })
        }

   }

 
    def payCart(user_id : String, user: User, wallet: Users, store:Store, promos : List[(Seq[String],String)], promo : Promotions): Future[List[String]] = {
        val method = new BucksAndShop(user = user, wallet = wallet)
        val ff: Future[Future[List[String]]]= 
            for {
                paidPromos <- handlePromos(method,promos, promo, store)
                products <- getCartFromUser(user_id)
            } yield {
                val (paid, notPaid) = products.partition(row => paidPromos.contains(row._4))
                var list_of_paid_products: List[String] = List()
                val paid_products_future = paid.foldLeft(Future(list_of_paid_products)) {
                    (f, row) => f.flatMap(l => removeProductFromCart(user_id, row._4).map(_ => row._1::l))
                }
                notPaid.foldLeft(paid_products_future) {
                    (f, product) => f.flatMap(l => handlePayment(product,user_id,method,store).map(_ => product._1::l))
                }
            }
        ff.flatMap(f => f)
    }

}