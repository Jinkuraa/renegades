package poca

import java.util.Date
import java.text.SimpleDateFormat
import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
import java.util.UUID
import scala.concurrent.Await
import scala.concurrent.duration.Duration

final case class AlreadyInPromotion(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)
final case class ImpossibleCase(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)

/* class Promotion
   * @id Integer, identifiant unique (non modifiable) de la promotion
   * @products Liste de Product, produits du site
   * @promos Short ∈ [0;100], pourcentage de promotion du bundle
   * @startPromo String au format "dd/MM/yyyy" indiquant le début de la promo
   * @endPromo String au format "dd/MM/yyyy" indiquant la fin de la promo
*/

case class Promotion(val promo_id: String, val products_id: String, val promo: Short, 
                    val startPromo: String, val endPromo: String)

class Promotions {
    class PromotionTable(tag: Tag) extends Table[(String, String, Short, String, String)](tag, "promo"){
        def promo_id = column[String]("promo_id")
        def products_id = column[String]("products_id")
        def promo = column[Short]("promo")
        def startPromo = column[String]("start_promo")
        def endPromo = column[String]("end_promo")
        def * = (promo_id, products_id, promo, startPromo, endPromo)
    }
    
    /* Va être utile pour comparer startPromo/endPromo à la date actuel : 
       obtenir la date actuel : standard.format(today) 
       renvoie une String de la forme "dd/MM/yyyy" */

    val standard: SimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy")
    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
    val db = MyDatabase.db
    val promotions = TableQuery[PromotionTable]

    /* Ajoute une promotion à la database */
    def makePromotion(promo_id: String, promo: Short, productIdList: List[String]): Future[Unit] = {
        productIdList.foldLeft(Future()) {
            (f, product_id) => f.flatMap { _ =>
                val isInPromotionFuture = isInPromotion(promo_id, product_id)
                isInPromotionFuture.flatMap(_ match {
                    case false => {
                        val newPromo = Promotion(promo_id=promo_id, products_id=product_id, promo=promo, startPromo="", endPromo="")
                        val newPromoAsTuple: (String, String, Short, String, String) = Promotion.unapply(newPromo).get
                        val dbio: DBIO[Int] = promotions += newPromoAsTuple
                        val resultFuture: Future[Int] = db.run(dbio)
                        resultFuture.map(_ => ())
                    }
                    case true => throw AlreadyInPromotion("Product already in promotion !")
                })
            }
        }
    }

    def createPromotionWithProduct(promo_id: String, product_id: String, promo_value: Short): Future[Unit] = {
        makePromotion(promo_id, promo_value, List(product_id))
    }

    def getAllPromo(): Future[Seq[Promotion]] = {
        val promoListFuture = db.run(promotions.result)

        promoListFuture.map((promoTuple: Seq[(String, String, Short, String, String)]) => {
            promoTuple.map(Promotion tupled _)
        })
    }

    def getAllProductsFromPromotion(promo_id: String): Future[Seq[String]] = {
        val query = promotions.filter(_.promo_id === promo_id)
        val query2 = query.map(_.products_id)
        db.run(query2.result)
    }

    /* Vérifie si un produit est déjà dispo dans la promotion */
    def isInPromotion(promo_id: String, product_id: String): Future[Boolean] = {
        val query = promotions.filter(_.promo_id === promo_id)
        val query2 = query.filter(_.products_id === product_id)
        val query3 = query2.map(_.promo)
        val promoFuture = db.run(query3.result)
        promoFuture.map(_.nonEmpty)
    }

    def removeAllPromotion(): Future[Unit] = {
        val removeFuture = db.run(promotions.delete)
        removeFuture.map(_ => ())
    }

    /* Supprime une promotion de la database */
    def removePromotion(promo_id: String): Future[Unit] = {
        val query = promotions.filter(_.promo_id === promo_id)
        val removeFuture = db.run(query.delete)
        removeFuture.map(_ => ())
    }

    /* Supprime un produit d'une promotion, doit être 
       modifier dans la database également*/
    def removeProductFromPromotion(promo_id: String, product_id: String): Future[Unit] = {
        val query = promotions.filter(_.promo_id === promo_id)
        val query2 = query.filter(_.products_id === product_id)
        val removeFuture = db.run(query2.delete)
        removeFuture.map(_ => ())
    }

    /* Renvoie combien la promotion fais économiser (pour le moment en RenegadeBucks) */
    def giveBenefits(promo_id : String): Future[Seq[Float]] = {
        val query = promotions.filter(_.promo_id === promo_id)
        val query2 = query.map(_.promo)
        val promoFuture = db.run(query2.result)
        promoFuture.map((promo: Seq[Short]) => promo.map(_.toFloat))
    }

    def allPromoFromProduct(product_id: String): Future[Seq[String]] = {
        val query = promotions.filter(_.products_id === product_id)
        val query2 = query.map(_.promo_id)
        db.run(query2.result)
    }


    /* Renvoie combien on doit payer après promotion appliquée (pour le moment en RenegadeBucks) */
    def giveTotalPriceWithPromotion(promo_id: String): Future[Float] = {
        val rawTotalFuture = db.run(sql"SELECT sum(products.price) FROM (products INNER JOIN promo ON products.product_id = promo.products_id) where promo_id = ${promo_id}".as[Float])
        for {
            total <- rawTotalFuture
            promo <- giveBenefits(promo_id)
        } yield {
            total(0) * (1 - (promo(0) / 100))
        }
    }

    /* Ajoute une promotion (donc un bundle entier) au panier 
      /!\ Cependant l'affichage doit être gérer différemment 
               dans le panier comme un bundle : "Jeu X + Jeu Y" 
               dans la même case. /!\ */
    def addPromotionToCart(promo_id: String, user_id: String, cart: Cart): Future[Unit] = {
        val allProductsFromPromotionFuture = getAllProductsFromPromotion(promo_id)
        allProductsFromPromotionFuture.flatMap(_.foldLeft(Future()) {
            (f, product_id) => f.flatMap(_ => cart.addProductToCart(user_id, product_id))
        })
    }
    
    private def listContainsEveryProductOfPromo(promo_id: String, productIdList : List[String]): Future[(Seq[String],Boolean)] = {
        var numberOfProductsInPromo = 0
        var mySeq : Seq[String] = Seq()
        var futureList = productIdList.foldLeft(Future(mySeq)) {(f,elt) =>
            f.flatMap(acc => {
                val isInPromoFuture = isInPromotion(promo_id, elt)
                isInPromoFuture.map(isInPromo => {
                    if(isInPromo) acc.appended(elt)
                    else acc
                })
            })
        }
        futureList.flatMap(proccessedMySeq => {
            val productsInPromoFuture = getAllProductsFromPromotion(promo_id)
            productsInPromoFuture.map(productsInPromo => {
                val snd = productsInPromo.size == proccessedMySeq.size
                val fst = proccessedMySeq.intersect(productsInPromo)
                (fst,snd)
            })
        })
    }

    
    private def noDuplicates(list: List[(Seq[String], String)]) = {
        list.distinctBy(_._2)
    }

    def listOfApplicablePromos(productIdList: List[String]): Future[List[(Seq[String], String)]] = {
        // produits, Produit ID
        val res: List[(Seq[String], String)] = List()
        val promoIdsProducts = productIdList.foldLeft(Future(res)) {(f, product) => {
            val futurePromos = allPromoFromProduct(product)

            futurePromos.flatMap(promos => 
                promos.foldLeft(f) {(f, promoId) => f.flatMap(l => {
                    val futurePromoApply = listContainsEveryProductOfPromo(promoId, productIdList)
                    futurePromoApply.map(couple => if (couple._2) (couple._1, promoId) :: l else l)
                })
            })
        }}

        promoIdsProducts.map(noDuplicates(_))
    }

    /* Démarre une promotion elle doit être visible sur le site,
       y compris dans la database */
    def startPromotion() = {
        /* Dans un premier temps version naive on démarre la promo 
           sans se soucier d'une quelconque date, et donc d'un 
           potentiel mécanisme de déclenchement. */
    }

    /* Fini une promotion elle ne doit plus être visible sur le site,
       ni dans la database */
    def endPromotion() = {
        /* A laisser vide pour le sprint courant pas utile. */
    }
}