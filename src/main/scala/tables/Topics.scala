
package poca

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
import java.util.UUID

case class Topic(topic_id: String, user_id: String, subject: String, author: String)

final case class TopicNotFoundException(private val message: String="", private val cause: Throwable=None.orNull)
  extends Exception(message, cause)

class Topics {
    class TopicsTable(tag: Tag) extends Table[(String, String, String, String)](tag, "topics") {
        def topic_id = column[String]("topic_id", O.PrimaryKey)
        def user_id = column[String]("user_id")
        def subject = column[String]("subject")
        def author = column[String]("author")
        def * = (topic_id, user_id, subject, author)
    }

    implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
    val db = MyDatabase.db
    val topics = TableQuery[TopicsTable]

    def createTopic(user_id: String, subject: String, author: String): Future[Unit] = {
        val topic_id = UUID.randomUUID.toString()
        val newTopic = Topic(topic_id=topic_id, user_id=user_id, subject=subject, author=author)
        val newTopicAsTuple: (String, String, String, String) = Topic.unapply(newTopic).get
        val dbio: DBIO[Int] = topics += newTopicAsTuple
        var resultFuture: Future[Int] = db.run(dbio)
        resultFuture.map(_ => ())
    }

    def getTopicById(topic_id: String): Future[Option[Topic]] = {
        val query = topics.filter(_.topic_id === topic_id)

        val topicListFuture = db.run(query.result)

        topicListFuture.map((topicList: Seq[(String, String, String, String)]) => {
            topicList.length match {
                case 0 => None
                case 1 => Some(Topic tupled topicList.head)
                case _ => throw new InconsistentStateException(s"Id $topic_id is linked to several topics in database!")
            }
        })
    }

    def deleteTopicById(topic_id: String): Future[Int] = {
        val query = topics.filter(_.topic_id === topic_id)
        val action = query.delete
        db.run(action)
    }

    def getAllTopics(): Future[Seq[Topic]] = {
        val topicListFuture = db.run(topics.result)

        topicListFuture.map((topicList: Seq[(String, String, String, String)]) => {
            topicList.map(Topic tupled _)
        })
    }
}
