package poca

import scala.concurrent.{Future, ExecutionContext}
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesPromotions(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

	def getPromoAdder(errors: List[String] = List()): ToResponseMarshallable = {
        logger.info("I got a request for Promo adder.")
        html.promo_adder(errors)
    }

    def addPromoProduct(fields: Map[String, String]): Future[ToResponseMarshallable] = {
        logger.info("I got a request to add promo.")

        Routes.get_fields(fields, "promo_id","product_id","promo") match {
            case Array(Some(promo_id), Some(product_id), Some(promo)) => {
                val promoCreation= content.promotion.createPromotionWithProduct(promo_id,product_id,promo.toShort)
                promoCreation.map(_ => {
                    getPromoAdder(List("Succesfully added the Product to the promo !"))
                }).recover({
                    case exc: GameKeyAlreadyExistsException => getPromoAdder(List("This code exists!"))
                    case exc: ProductNotFoundException => getPromoAdder(List("Product not found !"))
                })
            }
            case Array(None, _ , _) =>
                throw EmptyFieldException("Impossible State, Promo id field can't be empty")
            case Array(_, None , _) =>
                throw EmptyFieldException("Impossible State, Product id field can't be empty")
            case Array(_, _ , None) =>
                throw EmptyFieldException("Impossible State, Promo value field can't be empty")
        }
    }

	val routes: Route =
		concat(
			path("promo_adder") { get { complete(getPromoAdder()) } },
			path("promo_adder") { (post & formFieldMap) { fields => complete(addPromoProduct(fields)) } }
		)
}