
package poca

import scala.concurrent.{Future, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesKey(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def getKeysAdder(errors: List[String] = List()): ToResponseMarshallable = {
        logger.info("I got a request for signup.")

        html.keys_adder(errors)
    }

    def addGameKeys(fields: Map[String, String]): Future[ToResponseMarshallable] = {
        logger.info("I got a request to add keys.")

        Routes.get_fields(fields, "code","product_id") match {
            case Array(Some(code), Some(product_id)) => {
                val keyCreation = content.gamekeys.createGameKey(code, product_id)
                keyCreation.map(_ => {
                    getKeysAdder(List("Succesfully added the key !"))
                }).recover({
                    case exc: GameKeyAlreadyExistsException => getKeysAdder(List("This code exists!"))
                    case exc: ProductNotFoundException => getKeysAdder(List("Product not found !"))
                })
            }
            case Array(None, None) =>
                throw EmptyFieldException("Impossible State, fields can't be empty")
            case Array(None, _) =>
                throw EmptyFieldException("Impossible State, code field can't be empty")
            case Array(_, None) =>
                throw EmptyFieldException("Impossible State, product_id field can't be empty")
        }
    }

    val routes: Route = 
        concat(
            path("keys_adder") { get { complete(getKeysAdder()) } },
            path("keys_adder") { (post & formFieldMap) { fields => complete(addGameKeys(fields)) } }
        )
}
