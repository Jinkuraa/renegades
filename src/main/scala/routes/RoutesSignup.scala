
package poca

import scala.concurrent.{Future, Await, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesSignup(routesIndex: RoutesIndex)(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def getSignup(errors: List[String] = List()): ToResponseMarshallable = {
        logger.info("I got a request for signup.")

        html.signup(errors)
    }

    def register(fields: Map[String, String]): Future[Route] = {
        logger.info("I got a request to register.")

        Routes.get_fields(fields, "username","password","email") match {

            case Array(Some(username), Some(password), Some(email)) => {
                val userCreation =
                    content.users.createUser(
                        username = username,
                        is_admin = false,
                        email = email,
                        password = password,
                        wallet = 0
                    )
                userCreation.flatMap(_ => {
                    val userFuture = content.users.getUserByUsername(username)
                    userFuture.map(_ match {
                        case Some(user) =>
                            Sessions.clear() { Sessions.set(user.user_id, user.username) {
                                complete(routesIndex.getIndex(user.username)) }}
                        case _ =>
                            throw UserNotExistsException("Impossible State, user must be in the database")
                    })
                }).recover({
                    case exc: UserAlreadyExistsException => 
                        complete(getSignup(List(s"$username is already taken !")))
                    case exc: AttributesAlreadyUsedByUserException => 
                        complete(getSignup(exc.getMessage.split(',').toList.map(_+" is already taken")))
                })
            }

            case _ => Future(complete(getSignup(Routes.emptyFieldsErrorList(fields))))

        }
    }

    val routes: Route = 
        concat(
            path("signup") { get { complete(getSignup()) } },
            path("register") { (post & formFieldMap) { fields => Await.result(register(fields), Duration.Inf) } }
        )

}
