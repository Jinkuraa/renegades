
package poca

import scala.concurrent.{Future, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesForum(routesIndex: RoutesIndex)(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def getForum(close: Boolean = false): Future[ToResponseMarshallable] = {
        logger.info("I got a request for forum.")

        val topicsSeqFuture = content.topics.getAllTopics()
        topicsSeqFuture.map(html.forum(_, close))
    }

    def createTopic(subject: String, user_id: String, username: String): Future[ToResponseMarshallable] = {
        logger.info("I got a request to create the topic.")

        val newTopic = content.topics.createTopic(
            user_id = user_id,
            subject = subject,
            author = username
        )
        newTopic.flatMap(_ => getForum())
    }

    def closeTopic(topic_id: String): Future[ToResponseMarshallable] = {
        logger.info("I got a request to close the topic.")

        for {
            x <- content.topics.deleteTopicById(topic_id)
            y <- content.messages.deleteMessagesByTopicId(topic_id)
        } yield {
            logger.info(s"Delete ${x} topic(s)")
            logger.info(s"Delete ${y} message(s)")
            getForum(close = true) 
        }
    }

    def createOrCloseTopic(fields: Map[String, String], user_id: String, username: String): Future[ToResponseMarshallable] = {
        Routes.get_fields(fields, "closeButton", "subject") match {
            case Array (Some (topic_id), None) => closeTopic(topic_id)
            case Array (None, Some(subjectTitle)) => createTopic(subjectTitle, user_id, username)
            case Array (Some (_), Some(_)) => throw InconsistentStateException("Impossible state, can't close and create topic in same time")
            case _ => throw EmptyFieldException("Impossible state, topic field can't be empty")
        }
    }

    def getForumTopic(topic_id: String, username: String): Future[ToResponseMarshallable] = {
        logger.info("I got a request for forum/topic.")

        val topicFuture = content.topics.getTopicById(topic_id)
        topicFuture.flatMap(_ match {

            case Some(t) => {
                val messageSeqFuture = content.messages.getMessagesByTopicId(t.topic_id)
                messageSeqFuture.map(ms => html.forum_topic(username, t, ms.getOrElse(List())))
            }

            case _ => throw TopicNotFoundException("Could not find the topic")
        })
    }

    def sendMessage(fields: Map[String, String], topic_id: String, user_id: String, username: String): Future[ToResponseMarshallable] = {
        logger.info("I got a request to send a message.")

        Routes.get_fields(fields, "message") match {
            case Array (Some(message)) => {
                val messageCreation = 
                    content.messages.createMessage(
                        topic_id = topic_id,
                        user_id = user_id,
                        content = message,
                        date_hour = Routes.currentDateAndTime()
                    )
                messageCreation.flatMap(_ => getForumTopic(topic_id, username))
            }

            case _ => throw EmptyFieldException("Impossible state, message field can't be empty")
        }
    }

    def getTopic(r: String): String = r.split("/")(0)

    val routes: Route = 
        concat(
            path("forum") { get { complete(getForum()) } },
            path("forum") { (post & formFieldMap) { fields => Sessions.getall(Sessions.id, Sessions.name) {
                case Array (Some(id), Some(name)) => 
                    complete(createOrCloseTopic(fields, id.value, name.value))
                case Array (Some(_), None) => 
                    throw InconsistentStateException("Impossible State, user must have a name")
                case Array (None, Some(_)) => 
                    throw InconsistentStateException("Impossible State, user must have an id")
                case _ => 
                    complete(routesIndex.getIndex("", List("Please login to create a topic.")))
            } } },
            pathPrefix("forum" / Remaining) { r =>
                val topic_id = getTopic(r)
                get { Sessions.getall(Sessions.name) {
                    case Array (Some(name)) => complete(getForumTopic(topic_id, name.value))
                    case _ => complete(getForumTopic(topic_id, ""))
                } }
            },
            pathPrefix("forum" / Remaining) { r => (post & formFieldMap) { fields => 
                Sessions.getall(Sessions.id, Sessions.name) {
                    case Array (Some(id), Some(name)) => 
                        complete(sendMessage(fields, getTopic(r), id.value, name.value))
                    case Array (Some(_), None) => 
                        throw InconsistentStateException("Impossible State, user must have a name")
                    case Array (None, Some(_)) => 
                        throw InconsistentStateException("Impossible State, user must have an id")
                    case _ => 
                        throw InconsistentStateException("Impossible State, user must be logged in")
                }
            } }
        )

}
