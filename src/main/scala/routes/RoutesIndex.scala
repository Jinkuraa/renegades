package poca

import scala.concurrent.{Future, Await, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesIndex(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def getIndex(username: String, errors: List[String] = List()): Future[ToResponseMarshallable] = {
        logger.info("I got a request to enter the index.")
                                 
        val productSeqFuture = content.products.getAllProducts()
        productSeqFuture.map(html.index(_, errors, username))
    }

    def disconnect(fields: Map[String, String]): Route = {
        logger.info("I got a request to disconnect.")

        Routes.get_fields(fields, "disconnectButton") match {
            case Array (Some(_)) => Sessions.clear() { complete(getIndex("")) }
            case _ => throw InconsistentStateException("Impossible State, can't close the session")
        }
    }
    
    def signin(fields: Map[String, String]): Future[Route] = {
        logger.info("I got a request to signin.")

        Routes.get_fields(fields, "username", "password") match {
            case Array (Some(username), Some(password)) => {
                val existingUsersFuture = content.users.getUserByUsername(username)
                existingUsersFuture.map(_ match {
                    case Some(user) => {
                        if(Hash.checkPassword(password, user.password)){
                            Sessions.set(user.user_id, user.username) { complete(getIndex(user.username)) }
                        } else {
                            complete(getIndex("", List("Incorrect username or password")))
                        }
                    }
                    case _ => complete(getIndex("", List("Incorrect username or password")))
                })
            }
            case Array (None, None) =>
                throw EmptyFieldException("Impossible state, fields can't be empty")
            case Array (_, None) =>
                throw EmptyFieldException("Impossible state, password field can't be empty")
            case Array (None,_) =>
                throw EmptyFieldException("Impossible state, username field can't be empty")
        }
    }

    val routes: Route = 
        concat(
            path("index") { get { Sessions.getall(Sessions.name) {
                case Array (Some(username)) => complete(getIndex(username.value))
                case _ => complete(getIndex(""))
            }}},
            path("signin") { (post & formFieldMap) { fields => Await.result(signin(fields), Duration.Inf) } },
            path("disconnect") { (post & formFieldMap) { fields => disconnect(fields) } }
        )
}
