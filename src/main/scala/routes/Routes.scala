
package poca

import scala.concurrent.{Future, ExecutionContext}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.{HttpEntity, HttpResponse, ContentTypes, StatusCodes}
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import java.util.Date;
import java.text.SimpleDateFormat

class Routes(content: Content) extends LazyLogging {

    implicit val executionContext = ExecutionContext.Implicits.global

    implicit val implicit_content = content

    val routesIndex = new RoutesIndex()
    val routesInfo = new RoutesInfo()
    val routesBucksShopWallet = new RoutesBucksShopWallet(routesIndex)
    val routesForum = new RoutesForum(routesIndex)
    val routesKey = new RoutesKey()
    val routesPayment = new RoutesPayment(routesIndex)
    val routesProducts = new RoutesProducts(routesIndex)
    val routesSearch = new RoutesSearch()
    val routesSignup = new RoutesSignup(routesIndex)
    val routesUser = new RoutesUser(routesIndex)
    val routesPromotions = new RoutesPromotions()
    val routesRemoveAllPromotions = new RoutesRemoveAllPromotions()

    val routes: Route =
        concat(
            routesPayment.routes,
            routesIndex.routes,
            routesInfo.routes,
            routesBucksShopWallet.routes,
            routesForum.routes,
            routesKey.routes,
            routesProducts.routes,
            routesSearch.routes,
            routesSignup.routes,
            routesUser.routes,
            routesPromotions.routes,
            routesRemoveAllPromotions.routes,
            path("resources"/ Remaining) { resource =>
                if(!resource.contains("data")){
                    getFromResource(resource)
                } else {
                   complete(Routes.getError404)
                }
            },
            path(Remaining) { _ => complete(Routes.getError404) }
        )
           
}


final case class EmptyFieldException(private val message: String="", private val cause: Throwable=None.orNull) extends Exception(message, cause)

object Routes extends LazyLogging {

    def simpleHttp(text: String): HttpEntity.Strict = {
        HttpEntity(ContentTypes.`text/html(UTF-8)`, text)
    }

    def errorHttpResponse(text: String)(implicit executor: ExecutionContext): HttpResponse = {
        HttpResponse(StatusCodes.BadRequest, entity=text)
    }

    def getErrorPage(message: String, error_code: Int): ToResponseMarshallable = {
        logger.info("Could not find what you were looking for.")

        HttpResponse(
            error_code,
            entity=simpleHttp(html.generic_error(message, error_code).toString())
        )
    }

    val getError404 = getErrorPage("Page not found.",404)

    def emptyFieldsErrorList(fields: Map[String, String]): List[String] = {
        fields.foldLeft(List[String]()) { (l, couple) => {
            if(couple._2.isEmpty) s"${couple._1} field was not found"::l else l
        } }
    }

    def take_nonEmpty(fields: Map[String, String]): Map[String, String] = {
        fields.filter(_._2.nonEmpty)
    }

    def get_fields(fields: Map[String, String], ss: String*): Array[Option[String]] = {
        val filtered_fields = take_nonEmpty(fields)
        ss.map(filtered_fields.get).toArray
    }

    def currentDateAndTime(): String = {
        val dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a")
        dateFormatter.format(new Date())
    }
}
