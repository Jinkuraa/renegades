
package poca

import scala.concurrent.{Future, Await, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesPayment(routesIndex: RoutesIndex)(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def receiptAll(username: String, products: List[Product]): Future[ToResponseMarshallable] = {
        var gamekeys_codes = products.map(product => {
            val gameKeyFuture = content.gamekeys.getAvailableKeyForThatProduct(product.product_id)
            val codeFuture = gameKeyFuture.map(_ match {
                case Some(k) => {
                    content.gamekeys.buyKey(username, k.gamekeys_id)
                    k.code
                }
                case None => throw KeyNotFoundException("Could not find the key")
            } )
            Await.result(codeFuture, Duration.Inf)
        })
        Future(html.receipt(gamekeys_codes))
    }

    def namesToProducts(namesFuture: Future[List[String]]): Future[List[Product]] = {
        namesFuture.map(_.map(name => {
            val seqProductFuture = content.products.getAllProductsLike(name.toLowerCase())
            val seqProduct = Await.result(seqProductFuture, Duration.Inf)
            seqProduct(0)
	} ) )
    }

    def paymentAll(user_id: String): Future[ToResponseMarshallable] = {
        val userFuture = content.users.getUserById(user_id)
        userFuture.flatMap(_ match {
            case Some(user) => {
                // je convertis les produit id en objets produits
                // je vérifie s'il ya des promos appliquables
                // je créess les objets promos
                
                val productsFuture = content.cart.getCartFromUser(user_id)
                productsFuture.map(products => {
                    val name_list = products.map(_._4)
                    val promosFuture = content.promotion.listOfApplicablePromos(name_list.toList)
                    promosFuture.map(promos => {
                        val namesListFuture = content.cart.payCart(user_id, user, content.users, content.store, promos, content.promotion)
                        val productsListFuture = namesToProducts(namesListFuture)
                        productsListFuture.flatMap(res => {
                            receiptAll(user.username, res)
                        })
                    })
                })
            }
            case None =>  throw UserNotExistsException("Could not find the user")
        })
    }

    def getMyCart(id_value : String): Future[ToResponseMarshallable] = {
        
        val ff: Future[Future[ToResponseMarshallable]] = for {
            cart <- content.cart.getCartFromUser(id_value)
            sum <- content.cart.getValueOfCartFromUser(id_value)
            listOfProducts <- content.cart.getCartIdsFromUser(id_value)
            promos <- content.promotion.listOfApplicablePromos(listOfProducts.toList)
            
        } yield {
            val res: List[(List[Product], Float)] = List()
            val info = promos.foldLeft(Future(res)) { (f, promo) => f.flatMap(l =>
                for {
                    products <- promo._1.foldLeft(Future(List(): List[Product])) {
                        (_f, product) => _f.flatMap(_l => 
                            content.products.getProductById(product).map(_.get::_l)
                        )
                    }
                    perCent <- content.promotion.giveBenefits(promo._2)
                } yield (products, perCent(0))::l
            )}
            info.map(html.shopping_cart(cart, _, sum(0)))
        }
        ff.flatMap(f => f)
    }

    def myOrders(user_id: String): Future[ToResponseMarshallable] = {
        val userFuture = content.users.getUserById(user_id)
        userFuture.flatMap(_ match {
            case Some(user) => 
                val gameKeysSeqFuture = content.gamekeys.getAllGameKeysOwnedByUser(user.username)
                gameKeysSeqFuture.map(html.my_orders(_))
            case _ => throw InconsistentStateException("Impossible State, user must be in database")
        })
    }

    val routes: Route = 
        concat(
            path("payment-all") {
                post { Sessions.getall(Sessions.id) {
                    case Array (Some(id)) => complete(paymentAll(id.value))
                    case _ => complete(routesIndex.getIndex("", List("Please login to pay your cart.")))
                }}
            },
            path("cart") {
                get { Sessions.getall(Sessions.id) {
                    case Array (Some(id)) => complete(getMyCart(id.value))
                    case _ => complete(routesIndex.getIndex("", List("Please login to get access to Cart.")))
                }}
            },
            path("my_orders") { 
                get { Sessions.getall(Sessions.id) {
                    case Array (Some(id)) => complete(myOrders(id.value))
                    case _ => complete(routesIndex.getIndex("", List("Please login to get access to My orders.")))
                }}
            }
        )
}
