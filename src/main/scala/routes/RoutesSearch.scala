
package poca

import scala.concurrent.{Future, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesSearch(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def getSearchResults(game_name : String): Future[ToResponseMarshallable] = {
        logger.info(s"Search request for the game : $game_name.")

        val productsSeqFuture = content.products.getAllProductsLike(game_name.toLowerCase())
        productsSeqFuture.map(html.search(_))
    }

    val routes: Route = 
        concat(
            path("search") { get { parameters("game_name") { g => complete(getSearchResults(g)) } } }
        )

}
