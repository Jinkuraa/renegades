
package poca

import scala.concurrent.{Future, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesInfo(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def getHello(): ToResponseMarshallable = {
        logger.info("I got a request to greet.")

        Routes.simpleHttp("<h1>Say hello to akka-http</h1>")
    }
    
    def getUsers: Future[ToResponseMarshallable] = {
        logger.info("I got a request to get user list.")

        val userSeqFuture = content.users.getAllUsers()
        userSeqFuture.map(html.users(_))
    }

    val routes: Route = 
        concat(
            path("hello") { get { complete(getHello) } },
            path("users") { get { complete(getUsers) } }
        )

}
