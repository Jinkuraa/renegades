package poca

import scala.concurrent.{Future, ExecutionContext}
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesBucksShopWallet(routesIndex: RoutesIndex)(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def reloadWallet(username: String, offer: Short): Future[ToResponseMarshallable] = {
        logger.info("I got a request to reloadWallet.")

        val userFuture = content.users.getUserByUsername(username)
        userFuture.map(_ match {
            case Some(user) => {
                val temporary_shop = new BucksAndShop(user = user, content.users)
                temporary_shop.addBucks(offer)
                routesIndex.getIndex(username)
            }
            case _ => throw UserNotExistsException("Impossible State, user must be in the database")
        })
    }

    def getBucksShop(): ToResponseMarshallable = {
        logger.info("I got a request to getBucksShop.")
        html.bucks_shop()
    }

    val routes: Route = 
        concat(
            path("reload" / IntNumber ) { offer =>
                get { 
                    if (offer < 5 && offer > 0) {
                        val offerShort: Short = offer.toShort
                        Sessions.getall(Sessions.name) {
                            case Array (Some(name)) =>
                                complete(reloadWallet(name.value, offerShort))
                            case _ =>
                                complete(routesIndex.getIndex("", List("Please login to access to RenegadeBucks Shop.")))
                        }
                    } else {
                        complete(Routes.getError404)
                    }
                }
            },
            path("renegade-bucks-shop") { get { complete(getBucksShop()) } }
        )
}
