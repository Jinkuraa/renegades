
package poca

import scala.concurrent.{Future, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesUser(routesIndex: RoutesIndex)(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def getUserProfile(user_id: String): Future[ToResponseMarshallable] = {
        logger.info("I got a request to enter the user profile.")

        val userFuture = content.users.getUserById(user_id)
        userFuture.map(_ match {
            case Some(user_object) => html.user_profile(user_object)
            case None => throw UserNotExistsException ("No user with the credentials for this session is in the database.")
        })
    }

    val routes: Route = 
        concat(
            path("my_profile") { get { Sessions.getall(Sessions.id, Sessions.name) {
                case Array (Some(id), Some(_)) => 
                    complete(getUserProfile(id.value))
                case Array (Some(_), None) => 
                    throw InconsistentStateException("Impossible State, user must have a name")
                case Array (None, Some(_)) => 
                    throw InconsistentStateException("Impossible State, user must have an id")
                case _ => 
                    complete(routesIndex.getIndex("", List("Please login to get access to your profile.")))
            } } }
        )

}
