
package poca

import scala.concurrent.{Future, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import TwirlMarshaller._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

class RoutesProducts(routesIndex: RoutesIndex)(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def getProduct(product_id : String, user_id : String): Future[ToResponseMarshallable] = {
        val productFuture = content.products.getProductById(product_id)
        productFuture.flatMap(_ match{
            case Some(p) => {
                val avFuture = content.gamekeys.isAvailableForThatProduct(product_id)
                if (user_id == "") {
                    avFuture.map(html.product(p, _, false, ""))
                } else {
                    val cFuture = content.cart.isInCart(user_id, product_id)
                    avFuture.flatMap(av => cFuture.map(c => html.product(p, av, c, "")))
                }
            }
            case _ => throw InconsistentStateException("Impossible state, product should exist")
        })
    }

    def addProductToCart(fields : Map[String, String], user_id : String): Future[ToResponseMarshallable] = {
        logger.info("I got a request to add a product to a cart")

        Routes.get_fields(fields, "product_id") match {
            case Array(Some(product_id)) => {
                for {
                    product <- content.products.getProductById(product_id)
                    av <- content.gamekeys.isAvailableForThatProduct(product_id)
                    user <- content.users.getUserById(user_id) 
                } yield {
                    (product, user) match {
                        case (Some (p), Some(u)) => {
                            if (av) content.cart.addProductToCart(u.user_id, p.product_id)
                            val s = if (av) "Succesfully added to cart" else "Product no longer available"
                            html.product(p, false, av, s)
                        }
                        case (Some(_), None) => throw InconsistentStateException("Impossible State, user should exist")
                        case (None, Some(_)) => throw InconsistentStateException("Impossible State, product should exist")
                        case _ => throw InconsistentStateException("Impossible State, product and user should exist")
                    }
                }
            }
            case _ => throw InconsistentStateException("Impossible State, product_id must exist")
        }
    }

    val routes: Route = 
        concat(
            pathPrefix("game-" ~ Remaining) { r => get {
                var sp = r.split("-")(0)
                Sessions.getall(Sessions.id) {
                    case Array (Some(id)) => complete(getProduct(sp, id.value))
                    case _ => complete(getProduct(sp, ""))
                }
            } },
            pathPrefix("game-" ~ Remaining) { _ =>
                (post & formFieldMap) { fields =>
                    Sessions.getall(Sessions.id) {
                        case Array (Some(id)) => complete(addProductToCart(fields, id.value))
                        case _ => complete(routesIndex.getIndex("", List("Please login to buy a game.")))
                    }
                }
            }
        )
}
