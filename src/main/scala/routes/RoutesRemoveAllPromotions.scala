
package poca

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}

class RoutesRemoveAllPromotions(implicit executor: ExecutionContext, content: Content) extends LazyLogging {

    def removeAllPromotions(): Future[ToResponseMarshallable] = {
        logger.info("I got a request to remove all promotions from data base")
        val removeAllPromo = content.promotion.removeAllPromotion()
        removeAllPromo.map(_ => Routes.simpleHttp("<p>All promotions have been successfully removed from the database.</p>"))
    }

    val routes: Route =
        concat(
            path("remove_all_promotions") { get { complete(removeAllPromotions) } }
        )
}
