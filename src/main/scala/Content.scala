
package poca

class Content(
    _users: Users,
    _products: Products,
    _messages: Messages,
    _topics: Topics,
    _gamekeys: GameKeys,
    _store: Store,
    _cart: Cart,
    _promotion: Promotions
) {

    val users = _users
    val products = _products
    val messages = _messages
    val topics = _topics
    val gamekeys = _gamekeys
    val store = _store
    val cart = _cart
    val promotion = _promotion

}
