package poca

import org.mindrot.jbcrypt.BCrypt

object Hash{

	/* Function that allows you to return the hash of the password */

	def bcryptHash(password : String) : String = {
		BCrypt.hashpw(password, BCrypt.gensalt());
	}

	// Function that returns true if the password matches the hasher password and false otherwise

	def checkPassword(password : String, passwordHash : String) : Boolean ={
		BCrypt.checkpw(password, passwordHash)
	}
}