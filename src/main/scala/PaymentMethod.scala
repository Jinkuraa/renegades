
package poca

trait PaymentMethod {

    def pay(amount: Float, numberOfKeys: Int, store: Store = null) : Boolean

}