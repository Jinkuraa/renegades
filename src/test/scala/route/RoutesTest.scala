package route

import scala.concurrent.Future
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import scala.concurrent.duration._
import akka.http.scaladsl.testkit.RouteTestTimeout
import akka.testkit.TestDuration
import poca._

class RoutesTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

    // the Akka HTTP route testkit does not yet support a typed actor system (https://github.com/akka/akka-http/issues/2036)
    // so we have to adapt for now
    lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    
        def createRoute() = {
            var mockUsers = mock[Users]
            var mockProducts = mock[Products]
            var mockMessages = mock[Messages]
            var mockTopics = mock[Topics]
            var mockGameKeys = mock[GameKeys]
            var mockStore = mock[Store]
            var mockCart = mock[Cart]
            var mockPromotion = mock[Promotions]
            var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
            routesUnderTest
        }

    test("Route GET /return an Error") {
        val routesUnderTest = createRoute()

        val request = HttpRequest(uri = "/")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.NotFound)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Page not found.")
        }
    }

    test("Route GET /resource of a resource that shouldn't be visible should return an error") {
        val routesUnderTest = createRoute()

        val request = HttpRequest(uri = "/resources/data/inserts.sql")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.NotFound)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Page not found.")
        }
    } 

    test("Route GET /resource/[Resource Name] should return the content of the resource") {
        val routesUnderTest = createRoute()

        val request = HttpRequest(uri = "/resources/css/generic.css")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            mediaType should ===(MediaTypes.`text/css`)
            entityAs[String] should include("body")
        }
    }

   
}
