package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import poca._
import akka.http.scaladsl.model.headers.{ Cookie, HttpCookie, `Set-Cookie`, HttpCookiePair }


class ForumRouteTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

    lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /forum should returns the forum page") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]
        
        val topicList = List(
            Topic(topic_id = "tid1", user_id = "uid1", subject = "lol", author = "WR7"),
            Topic(topic_id = "tid2", user_id = "uid2", subject = "xd", author = "WR8"),
            Topic(topic_id = "tid3", user_id = "uid3", subject = "mdr", author = "WR9"),
            Topic(topic_id = "tid4", user_id = "uid4", subject = "haha", author = "WR10")
        )
        (mockTopics.getAllTopics _).expects().returning(Future((topicList))).once()
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(uri = "/forum")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("haha")
        }
    }

    test("Route POST /forum should return the index page without products when user is not logged in") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]
        (mockProducts.getAllProducts _).expects().returning(Future((List()))).once()
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum",
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Please login to create a topic")
        }
    }

    test("Route POST /forum should throw an exception when user is logged in but his id is None") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /forum should throw an exception when user is logged in but his name is None") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum",
            headers = List(Cookie(Sessions.id, value = "id_WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /forum should throw an exception when the subject field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum",
            entity = FormData(("subject","")).toEntity,
            headers = List(Cookie(Sessions.id, value = "id_WR7"), Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /forum should throw an exception when we are the subject and the closeButton fields") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum",
            entity = FormData(("closeButton", "foo"), ("subject","bar")).toEntity,
            headers = List(Cookie(Sessions.id, value = "id_WR7"), Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /forum should create a new topic") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]
        (mockTopics.createTopic _).expects("id_WR7", "Eat Ice", "WR7").returning(Future(())).once()
        val topicList = List(
            Topic(topic_id = "tid1", user_id = "id_WR7", subject = "Eat Ice", author = "WR7"),
            Topic(topic_id = "tid2", user_id = "uid2", subject = "xd", author = "WR8"),
            Topic(topic_id = "tid3", user_id = "uid3", subject = "mdr", author = "WR9"),
            Topic(topic_id = "tid4", user_id = "uid4", subject = "haha", author = "WR10")
        )
        (mockTopics.getAllTopics _).expects().returning(Future(topicList)).once()
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum",
            entity = FormData(("subject","Eat Ice")).toEntity,
            headers = List(Cookie(Sessions.id, value = "id_WR7"), Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Eat Ice")
        }
    }

    test("Route POST /forum should close the topic"){
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        (mockMessages.deleteMessagesByTopicId _).expects("id").returning(Future(0)).once()
        var mockTopics = mock[Topics]
        var mockPromotion = mock[Promotions]
        val topicList = List(
            Topic(topic_id = "tid1", user_id = "uid1", subject = "lol", author = "WR7"),
            Topic(topic_id = "tid2", user_id = "uid2", subject = "xd", author = "WR8"),
            Topic(topic_id = "tid3", user_id = "uid3", subject = "mdr", author = "WR9"),
            Topic(topic_id = "tid4", user_id = "uid4", subject = "haha", author = "WR10")
        )
        (mockTopics.getAllTopics _).expects().returning(Future(topicList))
        (mockTopics.deleteTopicById _).expects("id").returning(Future(1)).once()
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum",
            entity = FormData(("closeButton", "id")).toEntity,
            headers = List(Cookie(Sessions.id, value = "id_WR7"), Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Your topic has been deleted")
            entityAs[String] should include("lol")
        }
    }

    test("""Route GET /forum/topic_id/subject should returns 
        the topic page with messages when user is not logged in""") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]
        val messageList: List[Message] = List(
            Message(message_id = "mid1", topic_id = "tid", user_id = "uid1", content = "haha", date_hour = ""),
            Message(message_id = "mid2", topic_id = "tid", user_id = "uid2", content = "lol", date_hour = ""),
            Message(message_id = "mid3", topic_id = "tid", user_id = "uid3", content = "mdr", date_hour = ""),
            Message(message_id = "mid4", topic_id = "tid", user_id = "uid4", content = "xd", date_hour = "")
        )
        (mockMessages.getMessagesByTopicId _).expects("tid").returns(Future(Some(messageList))).once()
        
        val topic = Topic(topic_id = "tid", user_id = "uid", subject = "test", author = "WR7")
        (mockTopics.getTopicById _).expects("tid").returns(Future(Some(topic))).once()

        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(uri = "/forum/tid/test")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should not include("Send")
            entityAs[String] should not include("Close")
            entityAs[String] should include("xd")
        }
    }

    test("""Route GET /forum_topic/topic_id/subject should returns 
        the topic page without messages when user is not logged in""") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockMessages.getMessagesByTopicId _).expects("tid").returns(Future(None)).once()
        
        val topic = Topic(topic_id = "tid", user_id = "uid", subject = "test", author = "WR7")
        (mockTopics.getTopicById _).expects("tid").returns(Future(Some(topic))).once()
        var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart,mockPromotion)).routes
        val request = HttpRequest(uri = "/forum/tid/test")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should not include("Send")
            entityAs[String] should not include("Close")
            entityAs[String] should include("MESSAGE")
        }
    }

    test("""Route GET /forum_topic/topic_id/subject should returns 
        the topic page without messages when user is logged in""") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockMessages.getMessagesByTopicId _).expects("tid").returns(Future(None)).once()
        
        val topic = Topic(topic_id = "tid", user_id = "uid", subject = "test", author = "WR8")
        (mockTopics.getTopicById _).expects("tid").returns(Future(Some(topic))).once()
        var mockPromotion = mock[Promotions]

        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            uri = "/forum/tid/test",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Send")
            entityAs[String] should not include("Close")
            entityAs[String] should include("MESSAGE")
        }
    }

    test("Route GET /forum/topic_id/subject should throw an exception when the topic doesn't exist") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockTopics.getTopicById _).expects("tid").returns(Future(None)).once()
        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(uri = "/forum/tid/test")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("""Route POST /forum_topic/topic_id/subject should throw 
            an exception when the user is not logged in""") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum/tid/test",
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("""Route POST /forum_topic/topic_id/subject should throw an 
            exception when the user is logged in but his name is None""") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum/tid/test",
            headers = List(Cookie(Sessions.id, value = "id_WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("""Route POST /forum_topic/topic_id/subject should throw an 
            exception when the user is logged in but his id is None""") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum/tid/test",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /forum_topic/topic_id/subject should throw an exception when the message field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum/tid/test",
            entity = FormData(("message","")).toEntity,
            headers = List(Cookie(Sessions.id, value = "id_WR7"), Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /forum/topic_id/subject should create a new message") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        val dateFormatter = new java.text.SimpleDateFormat("dd/MM/yyyy hh:mm:ss a")
        val date_hour = dateFormatter.format(new java.util.Date())
        (mockMessages.createMessage _).expects("tid", "id_WR7", "haha", date_hour).returning(Future(())).once()
        val messageList: List[Message] = List(
            Message(message_id = "mid1", topic_id = "tid", user_id = "id_WR7", content = "haha", date_hour = ""),
            Message(message_id = "mid2", topic_id = "tid", user_id = "uid2", content = "lol", date_hour = ""),
            Message(message_id = "mid3", topic_id = "tid", user_id = "uid3", content = "mdr", date_hour = ""),
            Message(message_id = "mid4", topic_id = "tid", user_id = "uid4", content = "xd", date_hour = "")
        )
        (mockMessages.getMessagesByTopicId _).expects("tid").returns(Future(Some(messageList))).once()
        var mockTopics = mock[Topics]
        val topic = Topic(topic_id = "tid", user_id = "uid", subject = "test", author = "WR7")
        (mockTopics.getTopicById _).expects("tid").returns(Future(Some(topic))).once()
        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/forum/tid/test",
            entity = FormData(("message", "haha")).toEntity,
            headers = List(Cookie(Sessions.id, value = "id_WR7"), Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Send")
            entityAs[String] should include("Close")
            entityAs[String] should include("haha")
        }
    }
}
