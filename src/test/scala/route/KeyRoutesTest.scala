package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import poca._

class KeyRoutesTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

	lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /keys_adder should be a working link") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(uri = "/keys_adder")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Code")
            entityAs[String] should include("Product ID")
        }
    }

    test("Route POST /keys_adder should create a new key") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockGameKeys.createGameKey _).expects("supercode", "id").returning(Future(())).once()

        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/keys_adder",
            entity = FormData(("code", "supercode"), ("product_id", "id")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Succesfully added the key !")
        } 
    }

    test("Route POST /keys_adder should warn the user when the key already exist") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]

        (mockGameKeys.createGameKey _).expects("supercode", "id").returns(Future({
            throw GameKeyAlreadyExistsException("the code supercode already exists for another GameKey")
        })).once()

        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/keys_adder",
            entity = FormData(("code", "supercode"), ("product_id", "id")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("This code exists!")
        } 
    }

    test("Route POST /keys_adder should warn the user when the product was not found") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]

        (mockGameKeys.createGameKey _).expects("supercode", "id").returns(Future({
            throw ProductNotFoundException("Product with product_id = id not found !")
        })).once()

        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/keys_adder",
            entity = FormData(("code", "supercode"), ("product_id", "id")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Product not found !")
        } 
    }

    test("Route POST /keys_adder should throw an exception when code field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/keys_adder",
            entity = FormData(("code", ""), ("product_id", "id")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        } 
    }

    test("Route POST /keys_adder should throw an exception when product_id field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/keys_adder",
            entity = FormData(("code", "supercode"), ("product_id", "")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        } 
    }

    test("Route POST /keys_adder should throw an exception when all fields are empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/keys_adder",
            entity = FormData(("code", ""), ("product_id", "")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        } 
    }
}