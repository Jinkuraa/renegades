package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import poca._

class PromotionsRoutesTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

	lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /promo_adder should returns the promo_adder page") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(uri = "/promo_adder")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Promo id")
            entityAs[String] should include("Product id")
            entityAs[String] should include("Promo value")
        }
    }

    test("Route POST /promo_adder should throw an exception when Promo id field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/promo_adder",
            entity = FormData(("promo_id", ""), ("product_id", "product_id"), ("promo", "50")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /promo_adder should throw an exception when Product id field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/promo_adder",
            entity = FormData(("promo_id", "promo_id"), ("product_id", ""), ("promo", "50")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /promo_adder should throw an exception when Promo value field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/promo_adder",
            entity = FormData(("promo_id", "promo_id"), ("product_id", "product_id"), ("promo", "")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /promo_adder should warn the user when the code already exist") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockPromotion.createPromotionWithProduct _).expects("promo_id", "product_id", 50.toShort).returning(Future(
        	throw new GameKeyAlreadyExistsException
        )).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/promo_adder",
            entity = FormData(("promo_id", "promo_id"), ("product_id", "product_id"), ("promo", "50")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Promo id")
            entityAs[String] should include("Product id")
            entityAs[String] should include("Promo value")
            entityAs[String] should include("This code exists!")
        }
    }

    test("Route POST /promo_adder should warn the user when the product was not found") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockPromotion.createPromotionWithProduct _).expects("promo_id", "product_id", 50.toShort).returning(Future(
        	throw new ProductNotFoundException
        )).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/promo_adder",
            entity = FormData(("promo_id", "promo_id"), ("product_id", "product_id"), ("promo", "50")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Promo id")
            entityAs[String] should include("Product id")
            entityAs[String] should include("Promo value")
            entityAs[String] should include("Product not found !")
        }
    }

    test("Route POST /promo_adder should add promo") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockPromotion.createPromotionWithProduct _).expects("promo_id", "product_id", 50.toShort).returning(Future(())).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/promo_adder",
            entity = FormData(("promo_id", "promo_id"), ("product_id", "product_id"), ("promo", "50")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Promo id")
            entityAs[String] should include("Product id")
            entityAs[String] should include("Promo value")
            entityAs[String] should include("Succesfully added the Product to the promo !")
        }
    }
}