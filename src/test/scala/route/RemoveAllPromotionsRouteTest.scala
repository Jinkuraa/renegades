package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import poca._

class RemoveAllPromotionsRoutesTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

	lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /remove_all_promotions should return the remove_all_promotions page") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockPromotion.removeAllPromotion _).expects().returns(Future(())).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(uri = "/remove_all_promotions")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should ===("<p>All promotions have been successfully removed from the database.</p>")
        }
    }
}