package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import poca._
import akka.http.scaladsl.model.headers.{ Cookie, HttpCookie, `Set-Cookie`, HttpCookiePair }
class ProductRoutesTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

    lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /game should throw an exception when product was not found") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockProducts.getProductById _).expects("id1").returns(Future(None)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(uri = "/game-id1-product")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route GET /game should show a game, user is not connected, no key is available") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val product = Product(product_id = "id1", product_name = "product",
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        (mockProducts.getProductById _).expects("id1").returns(Future(Some(product))).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(false)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(uri = "/game-id1-product")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("product")
            entityAs[String] should include("disabled")
        }
    }

    test("Route GET /game should show a game, user is not connected, a key is available") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val product = Product(product_id = "id1", product_name = "product",
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        (mockProducts.getProductById _).expects("id1").returns(Future(Some(product))).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(true)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(uri = "/game-id1-product")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("product")
            entityAs[String] should include("submit")
        }
    }

    test("Route GET /game should show a game, user is connected, no key is available") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val product = Product(product_id = "id1", product_name = "product",
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        (mockProducts.getProductById _).expects("id1").returns(Future(Some(product))).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(false)).once()
        (mockCart.isInCart _).expects("id_WR7", "id1").returns(Future(false)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("product")
            entityAs[String] should include("disabled")
        }
    }

    test("Route GET /game should show a game, user is connected, a key is available") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val product = Product(product_id = "id1", product_name = "product",
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        (mockProducts.getProductById _).expects("id1").returns(Future(Some(product))).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(true)).once()
        (mockCart.isInCart _).expects("id_WR7", "id1").returns(Future(false)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("product")
            entityAs[String] should include("submit")
        }
    }

    test("Route GET /game should show a game, user is connected, the user already added the key, a key is available") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val product = Product(product_id = "id1", product_name = "product",
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        (mockProducts.getProductById _).expects("id1").returns(Future(Some(product))).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(true)).once()
        (mockCart.isInCart _).expects("id_WR7", "id1").returns(Future(true)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("product")
            entityAs[String] should include("In cart")
        }
    }

    test("Route GET /game should show a game, user is connected, the user already added the key, a key is not available") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val product = Product(product_id = "id1", product_name = "product",
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        (mockProducts.getProductById _).expects("id1").returns(Future(Some(product))).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(false)).once()
        (mockCart.isInCart _).expects("id_WR7", "id1").returns(Future(true)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("product")
            entityAs[String] should include("disabled")
            entityAs[String] should include("In cart")
        }
    }

    test("Route POST /game should return the index page when user is not logged in") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockProducts.getAllProducts _).expects().returns(Future(List())).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST, 
            uri = "/game-id1-product"
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Renegade-Gaming")
            entityAs[String] should include("Please login to buy a game.")
        }
    }

    test("Route POST /game should throw an exception when product field is None") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7")),
            entity = FormData(("product_id","")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /game should add the key to the cart") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val product = Product(product_id = "id1", product_name = "product",
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password="boss", wallet=0, email="wr7@boss.com")

        (mockProducts.getProductById _).expects("id1").returns(Future(Some(product))).once()
        (mockUsers.getUserById _).expects("id_WR7").returns(Future(Some(user))).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(true)).once()
        (mockCart.addProductToCart _).expects("id_WR7", "id1").returns(Future(())).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7")),
            entity = FormData(("product_id","id1")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("product")
            entityAs[String] should include("In cart")
            entityAs[String] should include("Succesfully added to cart")
        }
    }

    test("Route POST /game should warn the user when no key is available") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val product = Product(product_id = "id1", product_name = "product",
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password="boss", wallet=0, email="wr7@boss.com")

        (mockProducts.getProductById _).expects("id1").returns(Future(Some(product))).once()
        (mockUsers.getUserById _).expects("id_WR7").returns(Future(Some(user))).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(false)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7")),
            entity = FormData(("product_id","id1")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("product")
            entityAs[String] should include("disabled")
            entityAs[String] should include("Product no longer available")
        }
    }

    test("Route POST /game throw an exception when product is None") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password="boss", wallet=0, email="wr7@boss.com")

        (mockProducts.getProductById _).expects("id1").returns(Future(None)).once()
        (mockUsers.getUserById _).expects("id_WR7").returns(Future(Some(user))).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(false)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7")),
            entity = FormData(("product_id","id1")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /game should throw an exception when user is None") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val product = Product(product_id = "id1", product_name = "product",
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        (mockProducts.getProductById _).expects("id1").returns(Future(Some(product))).once()
        (mockUsers.getUserById _).expects("id_WR7").returns(Future(None)).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(false)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7")),
            entity = FormData(("product_id","id1")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /game should throw an exception when product and user are None") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockProducts.getProductById _).expects("id1").returns(Future(None)).once()
        (mockUsers.getUserById _).expects("id_WR7").returns(Future(None)).once()
        (mockGameKeys.isAvailableForThatProduct _).expects("id1").returns(Future(false)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/game-id1-product",
            headers = List(Cookie(Sessions.id, value = "id_WR7")),
            entity = FormData(("product_id","id1")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }
}
