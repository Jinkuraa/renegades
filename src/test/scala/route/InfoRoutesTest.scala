package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import poca._

class InfoRouteTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

	lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

	test("Route GET /hello should say hello") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(uri = "/hello")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should ===("<h1>Say hello to akka-http</h1>")
        }
    }

    test("Route GET /users should display the list of users") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockTopics = mock[Topics]

        val userList = List(
            User(user_id="id1", is_admin=false, username="riri", email="toto@yoyo.com", password="papa", wallet=0),
            User(user_id="id2", is_admin=false, username="fifi", email="toto@yoyo.com", password="popo", wallet=0),
            User(user_id="id2", is_admin=false, username="lulu", email="toto@yoyo.com", password="pupu", wallet=0)
        )
        (mockUsers.getAllUsers _).expects().returns(Future(userList)).once()

        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(uri = "/users")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("riri")
            entityAs[String] should include("fifi")
            entityAs[String] should include("lulu")
        }
    }
}