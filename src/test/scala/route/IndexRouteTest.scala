package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods, DateTime}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import akka.http.scaladsl.model.headers.{HttpCookie, `Set-Cookie`, Cookie}
import java.util.Date;
import poca._

class IndexRouteTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

    lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /index should return the index page without products when user is disconnected"){
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockProducts.getAllProducts _).expects().returning(Future(List())).once()
        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(uri = "/index")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Renegade-Gaming")
            entityAs[String] should include("Signin")
            entityAs[String] should not include("Disconnection")
        }
    }

    test("Route GET /index should return the index page with products when user is disconnected"){
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        val productList = List(
            Product(product_id = "id1", product_name = "product1", 
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner"),
            Product(product_id = "id2", product_name = "product2", 
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner"),
            Product(product_id = "id3", product_name = "product3", 
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner"),
        )
        (mockProducts.getAllProducts _).expects().returns(Future(productList)).once()
        
        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(uri = "/index")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Renegade-Gaming")
            entityAs[String] should include("Signin")
            entityAs[String] should not include("Disconnection")
            entityAs[String] should include("product1")
            entityAs[String] should include("product2")
            entityAs[String] should include("product3")
        }
    }

    test("Route GET /index should return the index page without products when user is logged in"){
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockProducts.getAllProducts _).expects().returning(Future(List())).once()
        var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes        
        val request = HttpRequest(
            uri = "/index",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Renegade-Gaming")
            entityAs[String] should not include("Signin")
            entityAs[String] should include("Disconnection")
        }
    }

    test("Route GET /index should return the index page with products when user is logged in"){
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        val productList = List(
            Product(product_id = "id1", product_name = "product1", 
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner"),
            Product(product_id = "id2", product_name = "product2", 
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner"),
            Product(product_id = "id3", product_name = "product3", 
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner"),
        )
        (mockProducts.getAllProducts _).expects().returns(Future(productList)).once()
       var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes        
        val request = HttpRequest(
            uri = "/index",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Renegade-Gaming")
            entityAs[String] should not include("Signin")
            entityAs[String] should include("Disconnection")
            entityAs[String] should include("product1")
            entityAs[String] should include("product2")
            entityAs[String] should include("product3")
        }
    }

    test("Route POST /signin should throw an exception when all fields are empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute() 

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/signin",
            entity = FormData(("username", ""),("password","")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /signin should throw an exception when the username field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute() 

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/signin",
            entity = FormData(("username", ""),("password","boss")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /signin should throw an exception when the password field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute() 
               
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/signin",
            entity = FormData(("username", "WR7"),("password","")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /signin should return index page when username is not correct") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockUsers.getUserByUsername _).expects("WR7").returns(Future(None)).once()
        (mockProducts.getAllProducts _).expects().returning(Future(List())).once()
        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes               
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/signin",
            entity = FormData(("username", "WR7"),("password","boss")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Incorrect username or password")
        }
    }

    test("Route POST /signin should return index page when password is not correct") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        val dummy_password = Hash.bcryptHash("lol")
        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password=dummy_password, wallet=0, email="wr7@boss.com")
        (mockUsers.getUserByUsername _).expects("WR7").returns(Future(Some(user))).once()
        (mockProducts.getAllProducts _).expects().returning(Future(List())).once()
        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes               
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/signin",
            entity = FormData(("username", "WR7"),("password","boss")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Incorrect username or password")
        }
    }

    test("Route POST /signin should connect the user") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        val dummy_password = Hash.bcryptHash("boss")
        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password=dummy_password, wallet=0, email="wr7@boss.com")
        (mockUsers.getUserByUsername _).expects("WR7").returning(Future((Some(user)))).once()
        (mockProducts.getAllProducts _).expects().returning(Future((List()))).once()
        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes               
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/signin",
            entity = FormData(("username", "WR7"),("password","boss")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            headers.collect{case `Set-Cookie`(x) => x } shouldEqual 
            List(HttpCookie(Sessions.id, value = "id_WR7"), HttpCookie(Sessions.name, value = "WR7"))
        }
    }

    test("Route POST /disconnect should return index page when user want to disconect") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockProducts.getAllProducts _).expects().returning(Future(List())).once()

       var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes               
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/disconnect",
            entity = FormData(("disconnectButton", "disconnect")).toEntity,
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Signin")
            entityAs[String] should not include("Disconnection")
            headers.collect{case `Set-Cookie`(x) => x } shouldEqual 
            List(HttpCookie(Sessions.id, value = "deleted", expires = Some(DateTime.MinValue)),
                HttpCookie(Sessions.name, value = "deleted", expires = Some(DateTime.MinValue)))
        }
    }

    test("""Route POST /disconnect should throw an exception if we are
        redirected to disconnect without pressing the button""") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()
               
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/disconnect",
            entity = FormData(("disconnectButton", "")).toEntity,
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }
}
