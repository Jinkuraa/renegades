package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import poca._

class SearchRoutesTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

	lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /search should be a working link") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]

        val productList = List(
            Product(product_id = "id1", product_name = "witcher", 
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner"),
            Product(product_id = "id2", product_name = "cyber", 
            release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner"),
        )
        (mockProducts.getAllProductsLike _).expects("e").returns(Future(productList)).once()

        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(uri = "/search?game_name=e")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("witcher")
            entityAs[String] should include("cyber")
        }
    }
}