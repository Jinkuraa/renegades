package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import akka.http.scaladsl.model.headers.{Cookie}
import poca._

class BucksShopWalletRoutesTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

	lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /renegade-bucks-shop should return the renegade-bucks-shop page") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(uri = "/renegade-bucks-shop")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)  
            entityAs[String] should include("RenegadeBucks Shop")  
        }
    }

    test("Route GET /reload/1 should return the index page when user is not logged in") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        
        (mockProducts.getAllProducts _).expects().returns(Future(List())).once()
        var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(uri = "/reload/1")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)  
            entityAs[String] should include("Please login to access to RenegadeBucks Shop")  
        }
    }

    test("Route GET /reload/1 should return the index page after purchase of 1000 bucks") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]

        val user = User(user_id="id_WR7", is_admin=false, username="WR7", email="wr7@boss.com", password="boss", wallet=0)
        (mockUsers.getUserByUsername _).expects("WR7").returns(Future(Some(user))).once()
        (mockUsers.updateWallet _).expects("WR7", 1000f).returns(Future(())).once()
        (mockProducts.getAllProducts _).expects().returns(Future(List())).once()

       var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            uri = "/reload/1",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)  
            entityAs[String] should include("Renegade-Gaming")  
        }
    }

    test("Route GET /reload/2 should return the index page after purchase of 2800 bucks") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]

        val user = User(user_id="id_WR7", is_admin=false, username="WR7", email="wr7@boss.com", password="boss", wallet=0)
        (mockUsers.getUserByUsername _).expects("WR7").returns(Future(Some(user))).once()
        (mockUsers.updateWallet _).expects("WR7", 2800f).returns(Future(())).once()
        (mockProducts.getAllProducts _).expects().returns(Future(List())).once()

       var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            uri = "/reload/2",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)  
            entityAs[String] should include("Renegade-Gaming")  
        }
    }

    test("Route GET /reload/3 should return the index page after purchase of 5000 bucks") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]

        val user = User(user_id="id_WR7", is_admin=false, username="WR7", email="wr7@boss.com", password="boss", wallet=0)
        (mockUsers.getUserByUsername _).expects("WR7").returns(Future(Some(user))).once()
        (mockUsers.updateWallet _).expects("WR7", 5000f).returns(Future(())).once()
        (mockProducts.getAllProducts _).expects().returns(Future(List())).once()

       var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            uri = "/reload/3",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)  
            entityAs[String] should include("Renegade-Gaming")  
        }
    }

    test("Route GET /reload/4 should return the index page after purchase of 13500 bucks") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]

        val user = User(user_id="id_WR7", is_admin=false, username="WR7", email="wr7@boss.com", password="boss", wallet=0)
        (mockUsers.getUserByUsername _).expects("WR7").returns(Future(Some(user))).once()
        (mockUsers.updateWallet _).expects("WR7", 13500f).returns(Future(())).once()
        (mockProducts.getAllProducts _).expects().returns(Future(List())).once()

       var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            uri = "/reload/4",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)  
            entityAs[String] should include("Renegade-Gaming")  
        }
    }

    test("Route GET /reload/1 should throw an exception when user doesn't exist") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]

        (mockUsers.getUserByUsername _).expects("WR7").returns(Future(None)).once()

       var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            uri = "/reload/1",
            headers = List(Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)  
        }
    }

    test("Route GET /reload/1 should return error page 404") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(uri = "/reload/12")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.NotFound)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)  
            entityAs[String] should include("Error 404")    
        }
    }
}