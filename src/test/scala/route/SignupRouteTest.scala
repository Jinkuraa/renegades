package route
import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods, DateTime}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import akka.http.scaladsl.model.headers.{HttpCookie, `Set-Cookie`, Cookie}
import poca._

class SignupRouteTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

    lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /signup should return the signup page"){
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(uri = "/signup")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("SIGNUP")
        }
    }

    test("Route POST /register should create a new user") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockUsers.createUser _).expects(false, "WR7", "wr7@boss.com", "boss", 0).returning(Future(())).once()
        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password="boss", wallet=0, email="wr7@boss.com")
        (mockUsers.getUserByUsername _).expects("WR7").returning(Future(Some(user))).once()
        (mockProducts.getAllProducts _).expects().returning(Future((List()))).once()
        
        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
            entity = FormData(("username", "WR7"), ("password","boss"), ("email","wr7@boss.com")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should not include("Signin")
            entityAs[String] should include("Disconnection")
            headers.collect{case `Set-Cookie`(x) => x } shouldEqual 
            List(HttpCookie(Sessions.id, value = "deleted", expires = Some(DateTime.MinValue)),
                HttpCookie(Sessions.name, value = "deleted", expires = Some(DateTime.MinValue)),
                HttpCookie(Sessions.id, value = "id_WR7"), 
                HttpCookie(Sessions.name, value = "WR7"))
        } 
    }

    test("Route POST /register should throw an exception when user doesn't exist in the database") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        (mockUsers.createUser _).expects(false, "WR7", "wr7@boss.com", "boss", 0).returning(Future(())).once()
        (mockUsers.getUserByUsername _).expects("WR7").returning(Future(None)).once()
        
       var mockPromotion = mock[Promotions]
        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
            entity = FormData(("username", "WR7"), ("password","boss"), ("email","wr7@boss.com")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        } 
    }

    test("Route POST /register should warn the user when username is already taken") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        (mockUsers.createUser _).expects(false, "WR7", "wr7@boss.com", "boss", 0).returns(Future({
            throw new UserAlreadyExistsException("A user with username WR7 already exists.")
        })).once()
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]

        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
            entity = FormData(("username", "WR7"), ("password", "boss"), ("email", "wr7@boss.com")).toEntity
        )
        request ~> routesUnderTest  ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("SIGNUP")
            entityAs[String] should include("WR7 is already taken !")
        }
    }

    test("Route POST /register should warn the user when the email is already taken") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        (mockUsers.createUser _).expects(false, "WR7", "wr7@boss.com", "boss", 0).returns(Future({
            throw new AttributesAlreadyUsedByUserException("email")
        })).once()
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]

        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes        
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
            entity = FormData(("username", "WR7"), ("password","boss"), ("email","wr7@boss.com")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("SIGNUP")
            entityAs[String] should include("email is already taken")
        }
    }

    test("Route POST /register should warn the user when the email and the username is already taken") {
        var mockUsers = mock[Users]
        var mockGameKeys = mock[GameKeys]
        (mockUsers.createUser _).expects(false, "WR7", "wr7@boss.com", "boss", 0).returns(Future({
            throw new AttributesAlreadyUsedByUserException("email,username")
        })).once()
        var mockProducts = mock[Products]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]

        var mockPromotion = mock[Promotions]
        var routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes        
        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
            entity = FormData(("username", "WR7"), ("password", "boss"), ("email", "wr7@boss.com")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("SIGNUP")
            entityAs[String] should include("email is already taken")
            entityAs[String] should include("username is already taken")
        }
    }

    test("Route POST /register should warn the user when the password field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()   

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
             entity = FormData(("username", "WR7"), ("password", ""), ("email", "wr7@boss.com")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("password field was not found")
        }
    }

    test("Route POST /register should warn the user when the username field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
             entity = FormData(("username", ""), ("password", "boss"), ("email", "wr7@boss.com")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("username field was not found")
        }
    }

    test("Route POST /register should warn the user when the email field is empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
             entity = FormData(("username", "WR7"), ("password", "boss"), ("email", "")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("email field was not found")
        } 
    }

    test("Route POST /register should warn the user when the username and password fields are empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
             entity = FormData(("username", ""), ("password", ""), ("email", "wr7@boss.com")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("username field was not found")
            entityAs[String] should include("password field was not found")
        } 
    }

    test("Route POST /register should warn the user when the username and email fields are empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
             entity = FormData(("username", ""), ("password", "boss"), ("email", "")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("username field was not found")
            entityAs[String] should include("email field was not found")
        } 
    }

    test("Route POST /register should warn the user when the password and email fields are empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
             entity = FormData(("username", "WR7"), ("password", ""), ("email", "")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("password field was not found")
            entityAs[String] should include("email field was not found")
        } 
    }

    test("Route POST /register should warn the user when all fields are empty") {
        val routetest = new RoutesTest()
        val routesUnderTest = routetest.createRoute()

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/register",
            entity = FormData(("username", ""), ("password", ""), ("email", "")).toEntity
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("username field was not found")
            entityAs[String] should include("password field was not found")
            entityAs[String] should include("email field was not found")
        } 
    }
}
