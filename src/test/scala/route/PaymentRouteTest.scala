package route

import org.scalatest.Matchers
import org.scalatest.funsuite.AnyFunSuite
import org.scalamock.scalatest.MockFactory
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model.{HttpRequest, StatusCodes, ContentTypes, MediaTypes, FormData, HttpMethods}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.concurrent.Future
import akka.http.scaladsl.model.headers.{ Cookie, HttpCookie, `Set-Cookie`, HttpCookiePair }
import poca._


class PaymentRouteTest extends AnyFunSuite with Matchers with MockFactory with ScalatestRouteTest {

    lazy val testKit = ActorTestKit()
    implicit def typedSystem = testKit.system
    override def createActorSystem(): akka.actor.ActorSystem =
        testKit.system.classicSystem

    test("Route GET /my_orders should return a game, user is logged in") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val ownedGames = Seq(("witcher", "img", "supercode", "today"))
        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password="boss", wallet=0, email="wr7@boss.com")

        (mockUsers.getUserById _).expects("id_WR7").returns(Future(Some(user))).once()
        (mockGameKeys.getAllGameKeysOwnedByUser _).expects("WR7").returns(Future(ownedGames)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            uri = "/my_orders",
            headers = List(Cookie(Sessions.id, value = "id_WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("witcher")
        }
    }

    test("Route GET /my_orders should throw an exception when user doesn't exist") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockUsers.getUserById _).expects("id_WR7").returns(Future(None)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            uri = "/my_orders",
            headers = List(Cookie(Sessions.id, value = "id_WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route GET /my_orders should return the index page when user is not logged in") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockProducts.getAllProducts _).expects().returns(Future(List())).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(uri = "/my_orders")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Please login to get access to My orders.")
        }
    }

    test("Route GET /cart should return the index page when user is not logged in") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockProducts.getAllProducts _).expects().returns(Future(List())).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(uri = "/cart")
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Please login to get access to Cart.")
        }
    }

    test("Route GET /cart should return the cart page when user is logged in empty cart") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]
        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password="boss", wallet=5000f, email="wr7@boss.com")
        val listOfProductName : List[String] = List()
        (mockCart.getCartFromUser _).expects("id_WR7").returns(Future(List())).once()
        (mockCart.getValueOfCartFromUser _).expects("id_WR7").returns(Future(Seq(0f))).once()
        (mockCart.getCartIdsFromUser _).expects("id_WR7").returns(Future(Seq())).once()
        (mockPromotion.listOfApplicablePromos _).expects(listOfProductName).returns(Future(List())).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(method = HttpMethods.GET ,uri = "/cart", headers = List(Cookie(Sessions.id, value = "id_WR7")))
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Shopping")
        }
    }

       test("Route GET /cart should return the cart page when user is logged in 1 Promo 1 Product") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]
        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password="boss", wallet=5000f, email="wr7@boss.com")
        val gameAddedToCart = Seq(("tw3", "img", 50f, "id_tw3"))
        val listOfProductName : List[String] = List("id_tw3")
        val produit = Product(product_id = "id_tw3", product_name = "tw3",
            release_date = "2323-12-12", description = "desc", price = 50f, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        (mockCart.getCartFromUser _).expects("id_WR7").returns(Future(gameAddedToCart)).once()
        (mockCart.getValueOfCartFromUser _).expects("id_WR7").returns(Future(Seq(50f))).once()
        (mockCart.getCartIdsFromUser _).expects("id_WR7").returns(Future(listOfProductName)).once()
        (mockPromotion.listOfApplicablePromos _).expects(listOfProductName).returns(Future(List((Seq("id_tw3"),"idpromo")))).once()
        (mockProducts.getProductById _).expects("id_tw3").returns(Future(Some(produit))).once()
        (mockPromotion.giveBenefits _).expects("idpromo").returns(Future(Seq(50f))).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(method = HttpMethods.GET ,uri = "/cart", headers = List(Cookie(Sessions.id, value = "id_WR7")))
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Shopping")
        }
    }

    test("Route POST /payment-all should return the index page when user is not logged in") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockProducts.getAllProducts _).expects().returns(Future(List())).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST,
            uri = "/payment-all"
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("Please login to pay your cart.")
        }
    }

    test("Route POST /payment-all should throw an exception when user doesn't exist") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        (mockUsers.getUserById _).expects("id_WR7").returns(Future(None)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST, 
            uri = "/payment-all", 
            headers = List(Cookie(Sessions.id, value = "id_WR7"), Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /payment-all should throw an exception when key was not found") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password="boss", wallet=5000f, email="wr7@boss.com")
        (mockUsers.getUserById _).expects("id_WR7").returns(Future(Some(user))).once()

        val gameAddedToCart = Seq(("tw3", "img", 15f, "id_tw3"))
        (mockCart.getCartFromUser _).expects("id_WR7").returns(Future(gameAddedToCart)).once()

        (mockPromotion.listOfApplicablePromos _).expects(List("id_tw3")).returns(Future(List())).once()
        (mockCart.payCart _).expects("id_WR7", user, mockUsers, mockStore, List(), mockPromotion).returns(Future(List("tw3"))).once()

        val product = Product(product_id = "id_tw3", product_name = "tw3",
            release_date = "2323-12-12", description = "desc", price = 15f, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        (mockProducts.getAllProductsLike _).expects("tw3").returns(Future(Seq(product))).once()
   
        (mockGameKeys.getAvailableKeyForThatProduct _).expects("id_tw3").returns(Future(None)).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST, 
            uri = "/payment-all", 
            headers = List(Cookie(Sessions.id, value = "id_WR7"), Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.InternalServerError)
            contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        }
    }

    test("Route POST /payment-all should pay the cart") {
        var mockUsers = mock[Users]
        var mockProducts = mock[Products]
        var mockGameKeys = mock[GameKeys]
        var mockMessages = mock[Messages]
        var mockTopics = mock[Topics]
        var mockStore = mock[Store]
        var mockCart = mock[Cart]
        var mockPromotion = mock[Promotions]

        val user = User(user_id="id_WR7", is_admin=false, username="WR7", password="boss", wallet=5000f, email="wr7@boss.com")
        (mockUsers.getUserById _).expects("id_WR7").returns(Future(Some(user))).once()

        val gameAddedToCart = Seq(("tw3", "img", 15f, "id_tw3"))
        (mockCart.getCartFromUser _).expects("id_WR7").returns(Future(gameAddedToCart)).once()

        (mockPromotion.listOfApplicablePromos _).expects(List("id_tw3")).returns(Future(List())).once()
        (mockCart.payCart _).expects("id_WR7", user, mockUsers, mockStore, List(), mockPromotion).returns(Future(List("tw3"))).once()

        val product = Product(product_id = "id_tw3", product_name = "tw3",
            release_date = "2323-12-12", description = "desc", price = 15f, os = "os", processor ="pc", 
            memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        (mockProducts.getAllProductsLike _).expects("tw3").returns(Future(Seq(product))).once()

        val gamekey = GameKey("id_keys", "02258895","id_tw3", true, "WR7", "18-09-2009 18:32")    
        (mockGameKeys.getAvailableKeyForThatProduct _).expects("id_tw3").returns(Future(Some(gamekey))).once()

        (mockGameKeys.buyKey _).expects("WR7","id_keys").returns(Future(())).once()

        val routesUnderTest = new Routes(new Content(mockUsers, mockProducts, mockMessages, mockTopics, mockGameKeys, mockStore, mockCart, mockPromotion)).routes

        val request = HttpRequest(
            method = HttpMethods.POST, 
            uri = "/payment-all", 
            headers = List(Cookie(Sessions.id, value = "id_WR7"), Cookie(Sessions.name, value = "WR7"))
        )
        request ~> routesUnderTest ~> check {
            status should ===(StatusCodes.OK)
            contentType should ===(ContentTypes.`text/html(UTF-8)`)
            entityAs[String] should include("YOUR RECEIPT")
            entityAs[String] should include("02258895")
        }
    }
}
