package database

import scala.util.{Success, Failure}
import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.meta._
import org.scalatest.{Matchers, BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatest.funsuite.AnyFunSuite
import com.typesafe.scalalogging.LazyLogging
import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import ch.qos.logback.classic.{Level, Logger}
import org.slf4j.LoggerFactory
import poca._

class DatabaseTest extends AnyFunSuite with Matchers with BeforeAndAfterAll with BeforeAndAfterEach with LazyLogging {
    val rootLogger: Logger = LoggerFactory.getLogger("com").asInstanceOf[Logger]
    rootLogger.setLevel(Level.INFO)
    val slickLogger: Logger = LoggerFactory.getLogger("slick").asInstanceOf[Logger]
    slickLogger.setLevel(Level.INFO)

    // In principle, mutable objets should not be shared between tests, because tests should be independent from each other. However for performance the connection to the database should not be recreated for each test. Here we prefer to share the database.
    override def beforeAll() {
        val isRunningOnCI = sys.env.getOrElse("CI", "") != ""
        val configName = if (isRunningOnCI) "myTestDBforCI" else "myTestDB"
        val config = ConfigFactory.load().getConfig(configName)
        MyDatabase.initialize(config)
    }
    override def afterAll() {
        MyDatabase.db.close
    }

    override def beforeEach() {
        val resetSchema = sqlu"drop schema public cascade; create schema public;"
        val resetFuture: Future[Int] = MyDatabase.db.run(resetSchema)
        Await.result(resetFuture, Duration.Inf)
        new RunMigrations(MyDatabase.db)()
    }

    test("Users.createUser should create a new user") {
        val users = new Users()

        val dummy_password = "password123";
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)
        createUserFuture.value should be(Some(Success(())))

        val getUsersFuture: Future[Seq[User]] = users.getAllUsers()
        val allUsers: Seq[User] = Await.result(getUsersFuture, Duration.Inf)

        allUsers.length should be(1)
        allUsers.head.username should be("toto")
    }

    test("Users.createUser should throw an exception because username and email exists") {
        val users = new Users()

        val dummy_password = "password123";
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)
        createUserFuture.value should be(Some(Success(())))

        val dummy_password2 = "password321";
        val createDuplicateUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password2,0)
        Await.ready(createDuplicateUserFuture, Duration.Inf)

        createDuplicateUserFuture.value match {
            case Some(Failure(exc: AttributesAlreadyUsedByUserException)) =>
                exc.getMessage should equal ("email,username")
            case _ => fail("should throw an exception")
        }
    }

    test("Users.createUser should throw an exception because username exist") {
        val users = new Users()

        val dummy_password = "password123";
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)
        createUserFuture.value should be(Some(Success(())))

        val dummy_password2 = "password321";
        val createDuplicateUserFuture: Future[Unit] = users.createUser(false,"toto","toto2@yoyo.com",dummy_password2,0)
        Await.ready(createDuplicateUserFuture, Duration.Inf)

        createDuplicateUserFuture.value match {
            case Some(Failure(exc: UserAlreadyExistsException)) =>
                exc.getMessage should equal ("A user with username 'toto' already exists.")
            case _ => fail("should throw an exception")
        }
    }

    test("Users.createUser should throw an exception because email exist") {
        val users = new Users()

        val dummy_password = "password123";
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)
        createUserFuture.value should be(Some(Success(())))

        val dummy_password2 = "password321";
        val createDuplicateUserFuture: Future[Unit] = users.createUser(false,"toto2","toto@yoyo.com",dummy_password2,0)
        Await.ready(createDuplicateUserFuture, Duration.Inf)

        createDuplicateUserFuture.value match {
            case Some(Failure(exc: AttributesAlreadyUsedByUserException)) =>
                exc.getMessage should equal ("email")
            case _ => fail("should throw an exception")
        }
    }

    test("Users.getUserByUsername should return a user because username exist") {
        val users = new Users()

        val dummy_password: String = "password123"
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)

        val returnedUserFuture: Future[Option[User]] = users.getUserByUsername("toto")
        val returnedUser: Option[User] = Await.result(returnedUserFuture, Duration.Inf)

        returnedUser match {
            case Some(user) => user.username should be("toto")
            case _ => fail("Should return a user.")
        }
    }

    test("Users.getUserByUsername should return None because username doesn't exist") {
        val users = new Users()

        val dummy_password: String = "password123"
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)

        val returnedUserFuture: Future[Option[User]] = users.getUserByUsername("somebody-else")
        val returnedUser: Option[User] = Await.result(returnedUserFuture, Duration.Inf)

        returnedUser should be(None)
    }

    test("Users.getUserByEmail should return a user because email exist") {
        val users = new Users()

        val dummy_password: String = "password123"
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)

        val returnedUserFuture: Future[Option[User]] = users.getUserByEmail("toto@yoyo.com")
        val returnedUser: Option[User] = Await.result(returnedUserFuture, Duration.Inf)

        returnedUser match {
            case Some(user) => user.email should be("toto@yoyo.com")
            case _ => fail("Should return a user.")
        }
    }

    test("Users.getUserByEmail should return None because email doesn't exist") {
        val users = new Users()

        val dummy_password: String = "password123"
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)

        val returnedUserFuture: Future[Option[User]] = users.getUserByEmail("toto2@yoyo.com")
        val returnedUser: Option[User] = Await.result(returnedUserFuture, Duration.Inf)

        returnedUser should be(None)
    }

    test("Users.getUserById should return a user because id exist") {
        val users = new Users()

        val dummy_password: String = "password123"
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)

        val returnedUserFuture: Future[Option[User]] = users.getUserByUsername("toto")
        val returnedUser: Option[User] = Await.result(returnedUserFuture, Duration.Inf)

        returnedUser match{
            case Some(user) => {
                val returnedUserFuture2: Future[Option[User]] = users.getUserById(user.user_id)
                val returnedUser2: Option[User] = Await.result(returnedUserFuture2, Duration.Inf)

                returnedUser2 match {
                    case Some(user2) => user2.user_id should be(user.user_id)
                    case None => fail("Should return a user.")
                }
            }
            case _ => fail("Should return a user.")
        }
    }

    test("Users.getUserById should return None because id doesn't exist") {
        val users = new Users()

        val dummy_password: String = "password123"
        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)

        val returnedUserFuture2: Future[Option[User]] = users.getUserById("id which doesn't exist")
        val returnedUser2: Option[User] = Await.result(returnedUserFuture2, Duration.Inf)

        returnedUser2 should be(None)
    }

    test("Users.getAllUsers should return a list of users") {
        val users = new Users()

        val dummy_password: String = "password123"
        val createUserFuture: Future[Unit] = users.createUser(false,"riri","toto@yoyo.com",dummy_password,0)
        Await.ready(createUserFuture, Duration.Inf)

        val createAnotherUserFuture: Future[Unit] = users.createUser(false,"fifi","toto@yoyo.zer",dummy_password,0)
        Await.ready(createAnotherUserFuture, Duration.Inf)

        val returnedUserSeqFuture: Future[Seq[User]] = users.getAllUsers()
        val returnedUserSeq: Seq[User] = Await.result(returnedUserSeqFuture, Duration.Inf)

        returnedUserSeq.length should be(2)
    }

    test("Users.getAllUsers should return empty list of users") {
        val users = new Users()

        val returnedUserSeqFuture: Future[Seq[User]] = users.getAllUsers()
        val returnedUserSeq: Seq[User] = Await.result(returnedUserSeqFuture, Duration.Inf)

        returnedUserSeq.length should be(0)
    }

    test("User.updateWallet should update the wallet of the user by a positive number") {
        val users = new Users()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",0)
        Await.ready(createUserFuture, Duration.Inf)

        val updateFuture: Future[Unit] = users.updateWallet("toto", 20f)
        Await.ready(updateFuture, Duration.Inf)

        val returnedUserFuture : Future[Option[User]] = users.getUserByUsername("toto")
        val returnedUser = Await.result(returnedUserFuture, Duration.Inf)

        returnedUser match {
            case Some(user) => user.wallet should equal (20f)
            case _ => fail("should update the wallet")
        }
    }

    test("User.updateWallet should update the wallet of the user by a negative number") {
        val users = new Users()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",20)
        Await.ready(createUserFuture, Duration.Inf)

        val updateFuture: Future[Unit] = users.updateWallet("toto", -15f)
        Await.ready(updateFuture, Duration.Inf)

        val returnedUserFuture : Future[Option[User]] = users.getUserByUsername("toto")
        val returnedUser = Await.result(returnedUserFuture, Duration.Inf)

        returnedUser match {
            case Some(user) => user.wallet should equal (5f)
            case _ => fail("should update the wallet")
        }
    }

    test("Messages.createMessage should create a new Message") {
        val messages = new Messages()
        val createMessageFuture: Future[Unit] = messages.createMessage("", "", "Bonjour", "")
        Await.ready(createMessageFuture, Duration.Inf)

        // Check that the future succeeds
        createMessageFuture.value should be(Some(Success(())))

        val getMessagesFuture: Future[Seq[Message]] = messages.getAllMessages()
        var allMessages: Seq[Message] = Await.result(getMessagesFuture, Duration.Inf)

        allMessages.length should be(1)
        allMessages.head.content should be("Bonjour")
    }

    test("Messages.getMessagesByTopicId should return a messages list") {
        val messages = new Messages()

        val createMessageFuture: Future[Unit] = messages.createMessage("id", "", "Bonjour", "")
        Await.ready(createMessageFuture, Duration.Inf)

        val createAnotherMessageFuture: Future[Unit] = messages.createMessage("id", "", "Salut", "")
        Await.ready(createAnotherMessageFuture, Duration.Inf)

        val createLastMessageFuture: Future[Unit] = messages.createMessage("id2", "", "lol", "")
        Await.ready(createLastMessageFuture, Duration.Inf)

        val returnedMessageSeqFuture: Future[Option[Seq[Message]]] = messages.getMessagesByTopicId("id")
        val returnedMessageSeq: Option[Seq[Message]] = Await.result(returnedMessageSeqFuture, Duration.Inf)

        returnedMessageSeq match{
            case Some(messageList) => messageList.length should be(2)
            case _ => fail("Should return an option message list")
        }
    }

    test("Messages.getMessagesByTopicId should return None") {
        val messages = new Messages()

        val createMessageFuture: Future[Unit] = messages.createMessage("id", "", "Bonjour", "")
        Await.ready(createMessageFuture, Duration.Inf)

        val createAnotherMessageFuture: Future[Unit] = messages.createMessage("id", "", "Salut", "")
        Await.ready(createAnotherMessageFuture, Duration.Inf)

        val createLastMessageFuture: Future[Unit] = messages.createMessage("id2", "", "lol", "")
        Await.ready(createLastMessageFuture, Duration.Inf)

        val returnedMessageSeqFuture: Future[Option[Seq[Message]]] = messages.getMessagesByTopicId("id3")
        val returnedMessageSeq: Option[Seq[Message]] = Await.result(returnedMessageSeqFuture, Duration.Inf)

        returnedMessageSeq should be(None)
    }

    test("Messages.deleteMessagesByTopicId should delete messages"){
        val messages = new Messages()

        val createMessageFuture: Future[Unit] = messages.createMessage("id1", "", "Bonjour", "")
        Await.ready(createMessageFuture, Duration.Inf)

        val createAnotherMessageFuture: Future[Unit] = messages.createMessage("id2", "", "Salut", "")
        Await.ready(createAnotherMessageFuture, Duration.Inf)

        val createLastMessageFuture: Future[Unit] = messages.createMessage("id2", "", "lol", "")
        Await.ready(createLastMessageFuture, Duration.Inf)

        val deleteMessageFuture: Future[Int] = messages.deleteMessagesByTopicId("id2")
        Await.ready(deleteMessageFuture, Duration.Inf)

        // Check that the future succeeds
        deleteMessageFuture.value should be(Some(Success(2)))

        val returnedMessageSeqFuture2: Future[Seq[Message]] = messages.getAllMessages()
        val returnedMessageSeq2: Seq[Message] = Await.result(returnedMessageSeqFuture2, Duration.Inf)

        returnedMessageSeq2.length should be(1)
    }

    test("Messages.deleteMessagesByTopicId shouldn't delete messages"){
        val messages = new Messages()

        val createMessageFuture: Future[Unit] = messages.createMessage("id1", "", "Bonjour", "")
        Await.ready(createMessageFuture, Duration.Inf)

        val deleteMessageFuture: Future[Int] = messages.deleteMessagesByTopicId("id2")
        Await.ready(deleteMessageFuture, Duration.Inf)

        // Check that the future succeeds
        deleteMessageFuture.value should be(Some(Success(0)))

        val returnedMessageSeqFuture2: Future[Seq[Message]] = messages.getAllMessages()
        val returnedMessageSeq2: Seq[Message] = Await.result(returnedMessageSeqFuture2, Duration.Inf)

        returnedMessageSeq2.length should be(1)
    }

    test("Messages.getAllMessages should return a list of messages") {
        val messages = new Messages()

        val createMessageFuture: Future[Unit] = messages.createMessage("", "", "Bonjour", "")
        Await.ready(createMessageFuture, Duration.Inf)

        val createAnotherMessageFuture: Future[Unit] = messages.createMessage("", "", "Salut", "")
        Await.ready(createAnotherMessageFuture, Duration.Inf)

        val returnedMessageSeqFuture: Future[Seq[Message]] = messages.getAllMessages()
        val returnedMessageSeq: Seq[Message] = Await.result(returnedMessageSeqFuture, Duration.Inf)

        returnedMessageSeq.length should be(2)
    }

    test("Messages.getAllMessages should return an empty list of messages") {
        val messages = new Messages()

        val returnedMessageSeqFuture: Future[Seq[Message]] = messages.getAllMessages()
        val returnedMessageSeq: Seq[Message] = Await.result(returnedMessageSeqFuture, Duration.Inf)

        returnedMessageSeq.length should be(0)
    }

    test("Topics.createTopic should create a new topic") {
        val topics = new Topics()

        val createTopicFuture: Future[Unit] = topics.createTopic("user_id","subject","author")
        Await.ready(createTopicFuture, Duration.Inf)

        // Check that the future succeeds
        createTopicFuture.value should be(Some(Success(())))

        val getTopicsFuture: Future[Seq[Topic]] = topics.getAllTopics()
        var allTopics: Seq[Topic] = Await.result(getTopicsFuture, Duration.Inf)

        allTopics.length should be(1)
    }

    test("Topics.getTopicById should return None") {
        val topics = new Topics()

        val createTopicFuture: Future[Unit] = topics.createTopic("user_id","subject","author")
        Await.ready(createTopicFuture, Duration.Inf)

        val returnedTopicFuture: Future[Option[Topic]] = topics.getTopicById("false_id")
        val returnedTopic: Option[Topic] = Await.result(returnedTopicFuture, Duration.Inf)

        returnedTopic should be(None)
    }

    test("Topics.getTopicById should return a topic") {
        val topics = new Topics()

        val createTopicFuture: Future[Unit] = topics.createTopic("user_id","subject","author")
        Await.ready(createTopicFuture, Duration.Inf)

        val returnedTopicSeqFuture: Future[Seq[Topic]] = topics.getAllTopics()
        val returnedTopicSeq: Seq[Topic] = Await.result(returnedTopicSeqFuture, Duration.Inf)

        val returnedTopicFuture2: Future[Option[Topic]] = topics.getTopicById(returnedTopicSeq.head.topic_id)
        val returnedTopic2: Option[Topic] = Await.result(returnedTopicFuture2, Duration.Inf)

        returnedTopic2 match {
            case Some(topic2) => topic2.subject should be("subject")
            case _ => fail("Should return a topic.")
        }
    }

    test("Topics.deleteTopicById should delete a topic"){
        val topics = new Topics()

        val createTopicFuture: Future[Unit] = topics.createTopic("user_id","subject","author")
        Await.ready(createTopicFuture, Duration.Inf)

        val returnedTopicSeqFuture: Future[Seq[Topic]] = topics.getAllTopics()
        val returnedTopicSeq: Seq[Topic] = Await.result(returnedTopicSeqFuture, Duration.Inf)

        val deleteTopicFuture: Future[Int] = topics.deleteTopicById(returnedTopicSeq.head.topic_id)
        Await.ready(deleteTopicFuture, Duration.Inf)

        // Check that the future succeeds
        deleteTopicFuture.value should be(Some(Success(1)))

        val returnedTopicSeqFuture2: Future[Seq[Topic]] = topics.getAllTopics()
        val returnedTopicSeq2: Seq[Topic] = Await.result(returnedTopicSeqFuture2, Duration.Inf)

        returnedTopicSeq2.length should be(0)
    }

    test("Topics.deleteTopicById shouldn't delete a topic"){
        val topics = new Topics()

        val createTopicFuture: Future[Unit] = topics.createTopic("user_id","subject","author")
        Await.ready(createTopicFuture, Duration.Inf)

        val deleteTopicFuture: Future[Int] = topics.deleteTopicById("id which doesn't exist")
        Await.ready(deleteTopicFuture, Duration.Inf)

        // Check that the future succeeds
        deleteTopicFuture.value should be(Some(Success(0)))

        val returnedTopicSeqFuture: Future[Seq[Topic]] = topics.getAllTopics()
        val returnedTopicSeq: Seq[Topic] = Await.result(returnedTopicSeqFuture, Duration.Inf)

        returnedTopicSeq.length should be(1)
    }

    test("Topics.getAllTopics should return a list of topics") {
        val topics = new Topics()

        val createTopicFuture: Future[Unit] = topics.createTopic("user_id","subject","author")
        Await.ready(createTopicFuture, Duration.Inf)

        val createAnotherTopicFuture: Future[Unit] = topics.createTopic("user_id2","subject2","author2")
        Await.ready(createAnotherTopicFuture, Duration.Inf)

        val returnedTopicSeqFuture: Future[Seq[Topic]] = topics.getAllTopics()
        val returnedTopicSeq: Seq[Topic] = Await.result(returnedTopicSeqFuture, Duration.Inf)

        returnedTopicSeq.length should be(2)
    }

    test("Topics.getAllTopics should return an empty list of topics") {
        val topics = new Topics()

        val returnedTopicSeqFuture: Future[Seq[Topic]] = topics.getAllTopics()
        val returnedTopicSeq: Seq[Topic] = Await.result(returnedTopicSeqFuture, Duration.Inf)

        returnedTopicSeq.length should be(0)
    }

    test("Products.createProduct should create a new product") {
        val products = new Products()

        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id1", 
            product_name = "product", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        // Check that the future succeeds
        createProductFuture.value should be(Some(Success(())))

        val returnedProductSeqFuture: Future[Seq[Product]] = products.getAllProducts()
        val returnedProductSeq: Seq[Product] = Await.result(returnedProductSeqFuture, Duration.Inf)

        returnedProductSeq.length should be(1)
    }

    test("Products.createProduct should throw an exception") {
        val products = new Products()

        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id1", 
            product_name = "product", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createDuplicateProductFuture: Future[Unit] = products.createProduct(product_id = "id1", 
            product_name = "product", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createDuplicateProductFuture, Duration.Inf)

        createDuplicateProductFuture.value match{
            case Some(Failure(exc: ProductAlreadyExistsException)) =>
                exc.getMessage should equal ("The product with id = id1 already exist")
            case _ => fail("Should throw an exception")
        }
    }

    test("Products.getProductById should return a Product") {
        val products = new Products()

        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id1", 
            product_name = "product", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val returnedProductFuture: Future[Option[Product]] = products.getProductById("id1")
        val returnedProduct: Option[Product] = Await.result(returnedProductFuture, Duration.Inf)

        returnedProduct match {
            case Some(product) => product.product_id should be("id1")
            case _ => fail("Should return a Product.")
        }
    }

    test("Products.getProductById should return None") {
        val products = new Products()

        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id1", 
            product_name = "product", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val returnedProductFuture: Future[Option[Product]] = products.getProductById("id2")
        val returnedProduct: Option[Product] = Await.result(returnedProductFuture, Duration.Inf)

        returnedProduct should be(None)
    }

    test("Products.getAllProducts should return a list of products") {
        val products = new Products()
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id1", 
            product_name = "product", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        Await.ready(createProductFuture, Duration.Inf)

        val createAnotherProductFuture: Future[Unit] = products.createProduct(
            product_id = "id2", product_name = "product", release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner")

        Await.ready(createAnotherProductFuture, Duration.Inf)

        val returnedProductSeqFuture: Future[Seq[Product]] = products.getAllProducts()
        val returnedProductSeq: Seq[Product] = Await.result(returnedProductSeqFuture, Duration.Inf)

        returnedProductSeq.length should be(2)
    }

    test("Products.getAllProducts should return an empty list of products") {
        val products = new Products()

        val returnedProductSeqFuture: Future[Seq[Product]] = products.getAllProducts()
        val returnedProductSeq: Seq[Product] = Await.result(returnedProductSeqFuture, Duration.Inf)

        returnedProductSeq.length should be(0)
    }

    test("Products.getProductsLike should return a list of products that match the like pattern") {
        val products = new Products()
        
        val createProductFuture1: Future[Unit] = products.createProduct(product_id = "id1", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")

        Await.ready(createProductFuture1, Duration.Inf)

        val createProductFuture2: Future[Unit] = products.createProduct(
            product_id = "id2", product_name = "cyberpunk", release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner")

        Await.ready(createProductFuture2, Duration.Inf)

        val createProductFuture3: Future[Unit] = products.createProduct(
            product_id = "id3", product_name = "dark souls", release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner")        

        Await.ready(createProductFuture3, Duration.Inf)

        val returnedProductSeqFuture: Future[Seq[Product]] = products.getAllProductsLike("e")
        val returnedProductSeq: Seq[Product] = Await.result(returnedProductSeqFuture, Duration.Inf)

        returnedProductSeq.length should be(2)
    }

    test("GameKeys.createGameKey should create a key") {
        val products = new Products()
        val gamekeys = new GameKeys()
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture: Future[Unit] = gamekeys.createGameKey("supercode", "id")
        Await.ready(createGameKeyFuture, Duration.Inf)
        createGameKeyFuture.value should be(Some(Success(())))

        val returnedGameKeySeqFuture: Future[Seq[GameKey]] = gamekeys.getAllGameKeys()
        val returnedGameKeySeq: Seq[GameKey] = Await.result(returnedGameKeySeqFuture, Duration.Inf)

        returnedGameKeySeq.length should be(1)
    }

    test("GameKeys.createGameKey should throw an exception because product doesn't exist") {
        val products = new Products()
        val gamekeys = new GameKeys()
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture: Future[Unit] = gamekeys.createGameKey("supercode", "false_id")
        Await.ready(createGameKeyFuture, Duration.Inf)

        createGameKeyFuture.value match {
            case Some(Failure(exc: ProductNotFoundException)) =>
                exc.getMessage should equal ("Product with product_id = false_id not found !")
            case _ => fail("Should throw an exception")
        }
    }

    test("GameKeys.createGameKey should throw an exception because code already used") {
        val products = new Products()
        val gamekeys = new GameKeys()
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id1", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createAnotherProductFuture: Future[Unit] = products.createProduct(product_id = "id2", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createAnotherProductFuture, Duration.Inf)

        val createGameKeyFuture: Future[Unit] = gamekeys.createGameKey("supercode", "id1")
        Await.ready(createGameKeyFuture, Duration.Inf)

        val createAnotherGameKeyFuture: Future[Unit] = gamekeys.createGameKey("supercode", "id2")
        Await.ready(createAnotherGameKeyFuture, Duration.Inf)

        createAnotherGameKeyFuture.value match {
            case Some(Failure(exc: GameKeyAlreadyExistsException)) =>
                exc.getMessage should equal ("the code supercode already exists for another GameKey")
            case _ => fail("Should throw an exception")
        }
    }

    test("GameKeys.getGameKeyByCode should give the code supercode") {
        val products = new Products()
        val gamekeys = new GameKeys()
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture1: Future[Unit] = gamekeys.createGameKey("supercode", "id")
        Await.ready(createGameKeyFuture1, Duration.Inf)

        val keyCodeFuture: Future[Option[GameKey]] = gamekeys.getGameKeyByCode("supercode")
        val keyCode: Option[GameKey] = Await.result(keyCodeFuture, Duration.Inf)

        keyCode match {
            case Some(k) => k.code should be("supercode")
            case None => fail("Should return a GameKey")
        }
    }

    test("GameKeys.getGameKeyByCode should return None") {
        val products = new Products()
        val gamekeys = new GameKeys()
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture1: Future[Unit] = gamekeys.createGameKey("supercode", "id")
        Await.ready(createGameKeyFuture1, Duration.Inf)

        val keyCodeFuture: Future[Option[GameKey]] = gamekeys.getGameKeyByCode("false_supercode")
        val keyCode: Option[GameKey] = Await.result(keyCodeFuture, Duration.Inf)

        keyCode should be(None)
    }

    test("GameKeys.isAvailableForThatProduct should return true") {
        val products = new Products()
        val gamekeys = new GameKeys()
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture: Future[Unit] = gamekeys.createGameKey("supercode", "id")
        Await.ready(createGameKeyFuture, Duration.Inf)

        val availableFuture: Future[Boolean] = gamekeys.isAvailableForThatProduct("id")
        val available: Boolean = Await.result(availableFuture, Duration.Inf)

        available should be(true)
    }

    test("GameKeys.isAvailableForThatProduct should return false") {
        val products = new Products()
        val gamekeys = new GameKeys()
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture: Future[Unit] = gamekeys.createGameKey("supercode", "id")
        Await.ready(createGameKeyFuture, Duration.Inf)

        val availableFuture: Future[Boolean] = gamekeys.isAvailableForThatProduct("false_id")
        val available: Boolean = Await.result(availableFuture, Duration.Inf)

        available should be(false)
    }

    test("GameKeys.getAllGameKeysOwnedByUser should return the number of games the user bought thanks to buyKey") {
        val products = new Products()
        val gamekeys = new GameKeys()
        val users = new Users()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",0)
        Await.ready(createUserFuture, Duration.Inf)
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture1: Future[Unit] = gamekeys.createGameKey("supercode1", "id")
        Await.ready(createGameKeyFuture1, Duration.Inf)

        val createGameKeyFuture2: Future[Unit] = gamekeys.createGameKey("supercode2", "id")
        Await.ready(createGameKeyFuture2, Duration.Inf)

        val keyFuture1: Future[Option[GameKey]] = gamekeys.getGameKeyByCode("supercode1")
        val key1: Option[GameKey] = Await.result(keyFuture1, Duration.Inf)

        val keyFuture2: Future[Option[GameKey]] = gamekeys.getGameKeyByCode("supercode2")
        val key2: Option[GameKey] = Await.result(keyFuture2, Duration.Inf)

        (key1,key2) match{
            case (Some(key1), Some(key2)) => {
                val buyFuture1: Future[Unit] = gamekeys.buyKey("toto", key1.gamekeys_id)
                Await.ready(buyFuture1, Duration.Inf)
                buyFuture1.value should be (Some(Success(())))

                val buyFuture2: Future[Unit] = gamekeys.buyKey("toto", key2.gamekeys_id)
                Await.ready(buyFuture2, Duration.Inf)
                buyFuture2.value should be (Some(Success(())))

                val ownedByUserFuture: Future[Seq[(String, String, String, String)]] = gamekeys.getAllGameKeysOwnedByUser("toto")
                val ownedByUser: Seq[(String, String, String, String)] = Await.result(ownedByUserFuture, Duration.Inf)

                ownedByUser.length should be (2)
            }
            case _ => fail("Should return two keys")
        }
    }

    test("GameKeys.getAllGameKeys should return a list of keys") {
        val products = new Products()
        val gamekeys = new GameKeys()
        
        val createProductFuture1: Future[Unit] = products.createProduct(product_id = "id1", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture1, Duration.Inf)

        val createProductFuture2: Future[Unit] = products.createProduct(
            product_id = "id2", product_name = "cyberpunk", release_date = "2323-12-12", description = "desc", price = 0, os = "os", processor ="pc", memory = "memory", 
            graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture2, Duration.Inf)

        val createGameKeyFuture1: Future[Unit] = gamekeys.createGameKey("supercode", "id1")
        Await.ready(createGameKeyFuture1, Duration.Inf)

        val createGameKeyFuture2: Future[Unit] = gamekeys.createGameKey("supercode2", "id2")     
        Await.ready(createGameKeyFuture2, Duration.Inf)

        val returnedGameKeySeqFuture: Future[Seq[GameKey]] = gamekeys.getAllGameKeys()
        val returnedGameKeySeq: Seq[GameKey] = Await.result(returnedGameKeySeqFuture, Duration.Inf)

        returnedGameKeySeq.length should be(2)
    }

    test("GameKeys.getAllGameKeys should return an empty list of keys") {
        val gamekeys = new GameKeys()

        val returnedGameKeySeqFuture: Future[Seq[GameKey]] = gamekeys.getAllGameKeys()
        val returnedGameKeySeq: Seq[GameKey] = Await.result(returnedGameKeySeqFuture, Duration.Inf)

        returnedGameKeySeq.length should be(0)
    } 

    test("GameKeys.getAvailableKeyForThatProduct should return a key") {
        val products = new Products()
        val gamekeys = new GameKeys()
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture: Future[Unit] = gamekeys.createGameKey("supercode", "id")
        Await.ready(createGameKeyFuture, Duration.Inf)

        val availableKeyFuture: Future[Option[GameKey]] = gamekeys.getAvailableKeyForThatProduct("id")
        val availableKey = Await.result(availableKeyFuture, Duration.Inf)

        availableKey match {
            case Some(k) => k.code should be ("supercode")
            case None => fail("Should return a key")
        }
    }

    test("GameKeys.buyKey should buy a key") {
        val products = new Products()
        val gamekeys = new GameKeys()
        val users = new Users()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",0)
        Await.ready(createUserFuture, Duration.Inf)
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture: Future[Unit] = gamekeys.createGameKey("supercode", "id")
        Await.ready(createGameKeyFuture, Duration.Inf)

        val keyFuture: Future[Option[GameKey]] = gamekeys.getGameKeyByCode("supercode")
        val key: Option[GameKey] = Await.result(keyFuture, Duration.Inf)

        key match{
            case Some(key) => {
                val buyFuture: Future[Unit] = gamekeys.buyKey("toto", key.gamekeys_id)
                Await.ready(buyFuture, Duration.Inf)
                buyFuture.value should be (Some(Success(())))

                val ownedByUserFuture: Future[Seq[(String, String, String, String)]] = gamekeys.getAllGameKeysOwnedByUser("toto")
                val ownedByUser: Seq[(String, String, String, String)] = Await.result(ownedByUserFuture, Duration.Inf)

                ownedByUser.length should be (1)
            }
            case _ => fail("Should return a key")
        }
    }

    test("GameKeys.buyKey shouldn't buy a key because it doesn't exist") {
        val products = new Products()
        val gamekeys = new GameKeys()
        val users = new Users()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",0)
        Await.ready(createUserFuture, Duration.Inf)
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createGameKeyFuture: Future[Unit] = gamekeys.createGameKey("supercode", "id")
        Await.ready(createGameKeyFuture, Duration.Inf)

        val buyFuture: Future[Unit] = gamekeys.buyKey("toto", "false_id")
        Await.ready(buyFuture, Duration.Inf)
        buyFuture.value should be (Some(Success(())))

        val ownedByUserFuture: Future[Seq[(String, String, String, String)]] = gamekeys.getAllGameKeysOwnedByUser("toto")
        val ownedByUser: Seq[(String, String, String, String)] = Await.result(ownedByUserFuture, Duration.Inf)

        ownedByUser.length should be (0)
    }

    test("Store.updateStore updates the store accordingly ") {
        val store = new Store()

        val updateStoreFuture: Future[Unit] = store.updateStore(20f, 1)
        Await.ready(updateStoreFuture, Duration.Inf)

        val storeFuture: Future[StoreC] = store.getStore()
        val sStore: StoreC = Await.result(storeFuture, Duration.Inf)

        assert(sStore.wallet == 20f && sStore.nb_of_sales == 1)
    }

    test("Store.getStore should return wallet = 0 and nb_of_sales = 0") {
        val store = new Store()

        val storeFuture: Future[StoreC] = store.getStore()
        val sStore: StoreC = Await.result(storeFuture, Duration.Inf)

        assert(sStore.wallet == 0f && sStore.nb_of_sales == 0)
    }

    test("Store.getStore should return wallet = 70 and nb_of_sales = 3") {
        val store = new Store()

        val updateStoreFuture: Future[Unit] = store.updateStore(20f, 1)
        Await.ready(updateStoreFuture, Duration.Inf)

        val updateStoreFuture2: Future[Unit] = store.updateStore(50f, 2)
        Await.ready(updateStoreFuture2, Duration.Inf)

        val storeFuture: Future[StoreC] = store.getStore()
        val sStore: StoreC = Await.result(storeFuture, Duration.Inf)

        assert(sStore.wallet == 70f && sStore.nb_of_sales == 3)
    }

    test("Cart.addProductToCart should add two products in the cart of the user") {
        val cart : Cart = new Cart()
        val users : Users = new Users()
        val products : Products = new Products()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",0)
        Await.ready(createUserFuture, Duration.Inf)
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createProductFuture2: Future[Unit] = products.createProduct(product_id = "id2", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture2, Duration.Inf)

        val getUserFuture = users.getUserByUsername("toto")
        val userOption = Await.result(getUserFuture, Duration.Inf)
        
        val user_id = userOption match { 
            case Some(u) => 
                val addProductToCartFuture = cart.addProductToCart(u.user_id, "id")
                Await.ready(addProductToCartFuture, Duration.Inf)

                val addProductToCartFuture2 = cart.addProductToCart(u.user_id, "id2")
                Await.ready(addProductToCartFuture2, Duration.Inf)

                val cartOfUserFuture = cart.getCartFromUser(u.user_id)
                val cartOfUser : Seq[(String, String, Float, String)]= Await.result(cartOfUserFuture, Duration.Inf)
                cartOfUser.length should be(2)
                assert(cartOfUser(0) === ("witcher", "String", 0, "id"))
                assert(cartOfUser(1) === ("witcher", "String", 0, "id2"))
            case _ => fail
        }


    }

    test("Cart.isInCart first product should not be in cart, the others yes") {
        val cart : Cart = new Cart()
        val users : Users = new Users()
        val products : Products = new Products()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",0)
        Await.ready(createUserFuture, Duration.Inf)
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createProductFuture2: Future[Unit] = products.createProduct(product_id = "id2", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture2, Duration.Inf)

        val getUserFuture = users.getUserByUsername("toto")
        val userOption = Await.result(getUserFuture, Duration.Inf)
        val user_id = userOption match { 
            case Some(u) => u.user_id
            case _ => fail
        }

        val addProductToCartFuture = cart.addProductToCart(user_id, "id")
        Await.ready(addProductToCartFuture, Duration.Inf)

        val addProductToCartFuture2 = cart.addProductToCart(user_id, "id2")
        Await.ready(addProductToCartFuture2, Duration.Inf)

        val isInCartFuture = cart.isInCart(user_id, "id1")
        val isInCart = Await.result(isInCartFuture, Duration.Inf)

        val isInCartFuture2 = cart.isInCart(user_id, "id")
        val isInCart2 = Await.result(isInCartFuture2, Duration.Inf)

        val isInCartFuture3 = cart.isInCart(user_id, "id2")
        val isInCart3 = Await.result(isInCartFuture3, Duration.Inf)

        assert(isInCart === false)
        assert(isInCart2 === true)
        assert(isInCart3 === true)
    }

    test("Cart.getCartIdsFromUser should return two products") {
        val cart : Cart = new Cart()
        val users : Users = new Users()
        val products : Products = new Products()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",0)
        Await.ready(createUserFuture, Duration.Inf)
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createProductFuture2: Future[Unit] = products.createProduct(product_id = "id2", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 0, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture2, Duration.Inf)

        val getUserFuture = users.getUserByUsername("toto")
        val userOption = Await.result(getUserFuture, Duration.Inf)
        
        userOption match { 
            case Some(u) => 
                val addProductToCartFuture = cart.addProductToCart(u.user_id, "id")
                Await.ready(addProductToCartFuture, Duration.Inf)

                val addProductToCartFuture2 = cart.addProductToCart(u.user_id, "id2")
                Await.ready(addProductToCartFuture2, Duration.Inf)

                val cartOfUserFuture = cart.getCartIdsFromUser(u.user_id)
                val cartOfUser : Seq[(String)] = Await.result(cartOfUserFuture, Duration.Inf)
                cartOfUser.length should be(2)
                assert(cartOfUser(0) === ("id"))
                assert(cartOfUser(1) === ("id2"))
            case _ => fail
        }
    }

    test("Cart.getValueOfCartFromUser should return the sum of two products added in the user's cart") {
        val cart : Cart = new Cart()
        val users : Users = new Users()
        val products : Products = new Products()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",0)
        Await.ready(createUserFuture, Duration.Inf)
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 20, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createProductFuture2: Future[Unit] = products.createProduct(product_id = "id2", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 15, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture2, Duration.Inf)

        val getUserFuture = users.getUserByUsername("toto")
        val userOption = Await.result(getUserFuture, Duration.Inf)
        
        userOption match { 
            case Some(u) => 
                val addProductToCartFuture = cart.addProductToCart(u.user_id, "id")
                Await.ready(addProductToCartFuture, Duration.Inf)

                val addProductToCartFuture2 = cart.addProductToCart(u.user_id, "id2")
                Await.ready(addProductToCartFuture2, Duration.Inf)

                val valueCartFuture = cart.getValueOfCartFromUser(u.user_id)
                val cartOfUser : Seq[(Float)] = Await.result(valueCartFuture, Duration.Inf)
                assert(cartOfUser(0) === 35)
            case _ => fail
        }
    }

    test("Cart.removeProductFromCart should remove a product") {
        val cart : Cart = new Cart()
        val users : Users = new Users()
        val products : Products = new Products()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",0)
        Await.ready(createUserFuture, Duration.Inf)
        
        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 20, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createProductFuture2: Future[Unit] = products.createProduct(product_id = "id2", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 15, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture2, Duration.Inf)

        val getUserFuture = users.getUserByUsername("toto")
        val userOption = Await.result(getUserFuture, Duration.Inf)
        
        userOption match { 
            case Some(u) => 
                val addProductToCartFuture = cart.addProductToCart(u.user_id, "id")
                Await.ready(addProductToCartFuture, Duration.Inf)

                val addProductToCartFuture2 = cart.addProductToCart(u.user_id, "id2")
                Await.ready(addProductToCartFuture2, Duration.Inf)

                val removeFuture = cart.removeProductFromCart(u.user_id, "id2")
                Await.ready(removeFuture, Duration.Inf)

                val cartOfUserFuture = cart.getCartFromUser(u.user_id)
                val cartOfUser : Seq[(String, String, Float, String)] = Await.result(cartOfUserFuture, Duration.Inf)
                cartOfUser.length should be(1)
                assert(cartOfUser(0) === ("witcher", "String", 20.0, "id"))

            case _ => fail
        }
    }

    test("Cart.payCart should pay the cart content") {
        val cart : Cart = new Cart()
        val users : Users = new Users()
        val products : Products = new Products()
        val promotions : Promotions = new Promotions()
        val store : Store = new Store()

        val createUserFuture: Future[Unit] = users.createUser(false,"toto","toto@yoyo.com","psw",100)
        Await.ready(createUserFuture, Duration.Inf)

        val getUserFuture = users.getUserByUsername("toto")
        val userOption = Await.result(getUserFuture, Duration.Inf)

        val user = userOption match {
            case Some(u) => u
            case _ => fail
        }

        val user_id = user.user_id

        val createProductFuture: Future[Unit] = products.createProduct(product_id = "id1",
            product_name = "game1", release_date = "2323-12-12", description = "desc", price = 30, os = "os",
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)
        val addProductToCartFuture = cart.addProductToCart(user_id, "id1")
        Await.ready(addProductToCartFuture, Duration.Inf)

        val createProductFuture2: Future[Unit] = products.createProduct(product_id = "id2",
            product_name = "game2", release_date = "2323-12-12", description = "desc", price = 40, os = "os",
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture2, Duration.Inf)
        val addProductToCartFuture2 = cart.addProductToCart(user_id, "id2")
        Await.ready(addProductToCartFuture2, Duration.Inf)

        val createProductFuture3: Future[Unit] = products.createProduct(product_id = "id3",
            product_name = "game3", release_date = "2323-12-12", description = "desc", price = 50, os = "os",
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture3, Duration.Inf)
        val addProductToCartFuture3 = cart.addProductToCart(user_id, "id3")
        Await.ready(addProductToCartFuture3, Duration.Inf)

        val promoCreateFuture: Future[Unit] = promotions.makePromotion("promoId", 50, List("id1", "id3"))
        Await.ready(promoCreateFuture, Duration.Inf)

        val payFuture: Future[List[String]] = cart.payCart(user_id, user, users, store, List((Seq("id1", "id3"), "promoId")), promotions)
        val res = Await.result(payFuture, Duration.Inf)

        val getUserFuture_bis = users.getUserByUsername("toto")
        val userOption_bis = Await.result(getUserFuture_bis, Duration.Inf)

        val wallet = userOption_bis match {
            case Some(u) => u.wallet
            case _ => fail
        }

        wallet should be(20)
        res.length should be(3)
    }

    test("Promotion.createPromotionWithProduct should create a promotion") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)
        promoFuture.value should be(Some(Success(())))

        val returnedPromoFuture: Future[Seq[Promotion]] = promotion.getAllPromo()
        val returnedPromo = Await.result(returnedPromoFuture, Duration.Inf)

        returnedPromo.length should be (1)
    }

    test("Promotion.createPromotionWithProduct should throw an exception because promotion already exist") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        promoFuture2.value match {
            case Some(Failure(exc: AlreadyInPromotion)) =>
                exc.getMessage should equal ("Product already in promotion !")
            case _ => fail("Should throw an exception")
        }
    }

    test("Promotion.getAllProductsFromPromotion should return an empty string list") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val returnedPromoFuture: Future[Seq[String]] = promotion.getAllProductsFromPromotion("false_id")
        val returnedPromo = Await.result(returnedPromoFuture, Duration.Inf)

        returnedPromo.length should be (0)
    }

    test("Promotion.getAllProductsFromPromotion should return a string list") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val promoFuture3: Future[Unit] = promotion.createPromotionWithProduct("promo_id3", "product_id3", 25.toShort)
        Await.ready(promoFuture3, Duration.Inf)

        val returnedPromoFuture: Future[Seq[String]] = promotion.getAllProductsFromPromotion("promo_id")
        val returnedPromo = Await.result(returnedPromoFuture, Duration.Inf)

        returnedPromo.length should be (2)
    }

    test("Promotion.isInPromotion should return true") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val isInPromoFuture: Future[Boolean] = promotion.isInPromotion("promo_id", "product_id")
        val isInPromo = Await.result(isInPromoFuture, Duration.Inf)

        isInPromo should equal (true)
    }

    test("Promotion.isInPromotion should return false") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val isInPromoFuture: Future[Boolean] = promotion.isInPromotion("promo_id", "product_id2")
        val isInPromo = Await.result(isInPromoFuture, Duration.Inf)

        isInPromo should equal (false)
    }

    test("Promotion.removeAllPromotion should remove all promotion") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val removePromotionFuture: Future[Unit] = promotion.removeAllPromotion()
        Await.ready(removePromotionFuture, Duration.Inf)

        val returnedPromoFuture: Future[Seq[Promotion]] = promotion.getAllPromo()
        val returnedPromo = Await.result(returnedPromoFuture, Duration.Inf)

        returnedPromo.length should be (0)
    }

    test("Promotion.removePromotion should remove a promotion") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val promoFuture3: Future[Unit] = promotion.createPromotionWithProduct("promo_id3", "product_id3", 25.toShort)
        Await.ready(promoFuture3, Duration.Inf)

        val removePromotionFuture: Future[Unit] = promotion.removePromotion("promo_id")
        Await.ready(removePromotionFuture, Duration.Inf)

        val returnedPromoFuture: Future[Seq[Promotion]] = promotion.getAllPromo()
        val returnedPromo = Await.result(returnedPromoFuture, Duration.Inf)

        returnedPromo.length should be (1)
    }

    test("Promotion.removePromotion shouldn't remove a promotion") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id2", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val removePromotionFuture: Future[Unit] = promotion.removePromotion("promo_id3")
        Await.ready(removePromotionFuture, Duration.Inf)

        val returnedPromoFuture: Future[Seq[Promotion]] = promotion.getAllPromo()
        val returnedPromo = Await.result(returnedPromoFuture, Duration.Inf)

        returnedPromo.length should be (2)
    }

    test("Promotion.removeProductFromPromotion should remove a product from a promotion") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val removePromotionFuture: Future[Unit] = promotion.removeProductFromPromotion("promo_id", "product_id")
        Await.ready(removePromotionFuture, Duration.Inf)

        val returnedPromoFuture: Future[Seq[Promotion]] = promotion.getAllPromo()
        val returnedPromo = Await.result(returnedPromoFuture, Duration.Inf)

        returnedPromo.length should be (1)
    }

    test("Promotion.removeProductFromPromotion shouldn't remove a product from a promotion") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val removePromotionFuture: Future[Unit] = promotion.removeProductFromPromotion("promo_id", "product_id3")
        Await.ready(removePromotionFuture, Duration.Inf)

        val returnedPromoFuture: Future[Seq[Promotion]] = promotion.getAllPromo()
        val returnedPromo = Await.result(returnedPromoFuture, Duration.Inf)

        returnedPromo.length should be (2)
    }

    test("Promotion.giveBenefits should return the value of a promotion") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val benefPromotionFuture: Future[Seq[Float]] = promotion.giveBenefits("promo_id")
        val benefPromotion = Await.result(benefPromotionFuture, Duration.Inf)

        benefPromotion should equal (Seq(50f, 25f))
    }

    test("Promotion.giveBenefits shouldn't return the value of a promotion because promotion doesn't exist") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val benefPromotionFuture: Future[Seq[Float]] = promotion.giveBenefits("promo_id2")
        val benefPromotion = Await.result(benefPromotionFuture, Duration.Inf)

        benefPromotion should equal (Seq())
    }

    test("Promotion.allPromoFromProduct should return a list of promotion for a product") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id2", "product_id", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val promoFuture3: Future[Unit] = promotion.createPromotionWithProduct("promo_id3", "product_id3", 25.toShort)
        Await.ready(promoFuture3, Duration.Inf)

        val productPromotionFuture: Future[Seq[String]] = promotion.allPromoFromProduct("product_id")
        val productPromotion = Await.result(productPromotionFuture, Duration.Inf)

        productPromotion should equal (Seq("promo_id", "promo_id2"))
    }

    test("Promotion.allPromoFromProduct should return an empty list of promotion for a product") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id2", "product_id", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val productPromotionFuture: Future[Seq[String]] = promotion.allPromoFromProduct("product_id2")
        val productPromotion = Await.result(productPromotionFuture, Duration.Inf)

        productPromotion should equal (Seq())
    }

    test("Promotion.giveTotalPriceWithPromotion should return the price after the application of a promotion") {
        val promotion = new Promotions()
        val products = new Products()

        val createProductFuture: Future[Unit] = products.createProduct(product_id = "product_id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 15f, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createAnotherProductFuture: Future[Unit] = products.createProduct(product_id = "product_id2", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 35, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createAnotherProductFuture, Duration.Inf)

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val promoFuture3: Future[Unit] = promotion.createPromotionWithProduct("promo_id3", "product_id3", 25.toShort)
        Await.ready(promoFuture3, Duration.Inf)

        val priceFuture: Future[Float] = promotion.giveTotalPriceWithPromotion("promo_id")
        val price = Await.result(priceFuture, Duration.Inf)

        price should equal (25f)
    }

    test("Promotion.addPromotionToCart should return the price after the application of a promotion") {
        val promotion = new Promotions()
        val cart = new Cart()
        val products = new Products()

        val createProductFuture: Future[Unit] = products.createProduct(product_id = "product_id", 
            product_name = "witcher", release_date = "2323-12-12", description = "desc", price = 15f, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createProductFuture, Duration.Inf)

        val createAnotherProductFuture: Future[Unit] = products.createProduct(product_id = "product_id2", 
            product_name = "witcher2", release_date = "2323-12-12", description = "desc", price = 35, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createAnotherProductFuture, Duration.Inf)

        val createLastProductFuture: Future[Unit] = products.createProduct(product_id = "product_id3", 
            product_name = "witcher3", release_date = "2323-12-12", description = "desc", price = 35, os = "os", 
            processor ="pc", memory = "memory", graphics="graph", storage="sto", image="String", partner="partner")
        Await.ready(createLastProductFuture, Duration.Inf)

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id2", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val promoFuture3: Future[Unit] = promotion.createPromotionWithProduct("promo_id3", "product_id3", 25.toShort)
        Await.ready(promoFuture3, Duration.Inf)

        val addPromotionFuture: Future[Unit] = promotion.addPromotionToCart("promo_id", "id_WR7", cart)
        Await.ready(addPromotionFuture, Duration.Inf)

        val addPromotionFuture2: Future[Unit] = promotion.addPromotionToCart("promo_id2", "id_WR7", cart)
        Await.ready(addPromotionFuture2, Duration.Inf)

        val cartOfUserFuture: Future[Seq[(String, String, Float, String)]] =  cart.getCartFromUser("id_WR7")
        val cartOfUser = Await.result(cartOfUserFuture, Duration.Inf)

        cartOfUser.length should be (2)
    }

    test("Promotion.listOfApplicablePromos should return a couple list, fst = list of product_id and snd = promo_id") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val promoFuture2: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id2", 25.toShort)
        Await.ready(promoFuture2, Duration.Inf)

        val promoFuture3: Future[Unit] = promotion.createPromotionWithProduct("promo_id3", "product_id3", 25.toShort)
        Await.ready(promoFuture3, Duration.Inf)

        val productIdList = List("product_id", "product_id2", "product_id3")
        val applicablePromoFuture: Future[List[(Seq[String], String)]] = promotion.listOfApplicablePromos(productIdList)
        val applicablePromo = Await.result(applicablePromoFuture, Duration.Inf)

        applicablePromo should equal (List( 
            (Seq("product_id3"), "promo_id3"),
            (Seq("product_id", "product_id2"), "promo_id")
        ))
    }

    test("Promotion.listOfApplicablePromos should return an empty couple list") {
        val promotion = new Promotions()

        val promoFuture: Future[Unit] = promotion.createPromotionWithProduct("promo_id", "product_id", 50.toShort)
        Await.ready(promoFuture, Duration.Inf)

        val productIdList = List("product_id4")
        val applicablePromoFuture: Future[List[(Seq[String], String)]] = promotion.listOfApplicablePromos(productIdList)
        val applicablePromo = Await.result(applicablePromoFuture, Duration.Inf)

        applicablePromo should equal (List())
    }
}