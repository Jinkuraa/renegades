package utilities

import org.scalatest.funsuite.AnyFunSuite
import poca.Hash

class HashTest extends AnyFunSuite {

	test("Hash.bcryptHash and Hash.checkPassword"){
		assert(Hash.checkPassword("lol", Hash.bcryptHash("lol")))
		assert(Hash.checkPassword("Wr7", Hash.bcryptHash("Wr7")))
		assert(Hash.checkPassword("I'm a boss", Hash.bcryptHash("I'm a boss")))
	}
}